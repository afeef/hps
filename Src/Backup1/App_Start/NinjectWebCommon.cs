[assembly: WebActivator.PreApplicationStartMethod(typeof(Ideaware.Hps.Web.Mvc.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(Ideaware.Hps.Web.Mvc.App_Start.NinjectWebCommon), "Stop")]

namespace Ideaware.Hps.Web.Mvc.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Modules;
    using NHibernate;
    using Ideaware.Hps.Services.Security.Contracts;
    using Ideaware.Hps.Services.Setup.Contracts;
    using Ideaware.Hps.Services.Stock.Contracts;
    using Ideaware.Hps.Services.Transactions.Contracts;
    using Ideaware.Hps.Services.Transactions;
    using Ideaware.Hps.Services.Stock;
    using Ideaware.Hps.Services.Setup;
    using Ideaware.Hps.Services.Security;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ISession>().ToMethod(x => MvcApplication.SessionFactory.GetCurrentSession());

            // Role, Membership and Froms Authentication providers
            kernel.Bind<IFormsAuthentication>().To<FormsAuthenticationService>();
            kernel.Bind<IRoleService>().To<RoleService>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<ISmtpClientService>().To<SmtpClientService>();

            //Setup services injection
            kernel.Bind<IBranchService>().To<BranchService>();
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<IColorService>().To<ColorService>();
            kernel.Bind<IDocumentTypeService>().To<DocumentTypeService>();
            kernel.Bind<IManufacturerService>().To<ManufacturerService>();
            kernel.Bind<IModelService>().To<ModelService>();
            kernel.Bind<IVendorService>().To<VendorService>();

            //Stock services injection

            kernel.Bind<IGoodsReceivedService>().To<GoodsReceivedService>();
            kernel.Bind<IGoodsTransferService>().To<GoodsTransferService>();
            kernel.Bind<IProductService>().To<ProductService>();

            //Transactions services injection

            kernel.Bind<IBookingService>().To<BookingService>();
            kernel.Bind<IInstallmentPlanService>().To<InstallmentPlanService>();
        }

        /*
        class ServiceModule : WcfModule
        {
            /// <summary>
            /// Loads the module into the kernel.
            /// </summary>
            public override void Load()
            {
                // Binding services InRequestScope allows a single instance to be shared for all requests to the Ninject kernel for
                // instances of that type in a given WCF REST Request.  Other options include:
                //   InTransientScope() - If you dont' want instances to be shared for a given WCF REST Request
                //   InSingletonScope() - If you want instances shared between all WCF REST Requests.  You of course need to handle thread safety in this case
                // You probably don't want to use InThreadScope() since IIS can re-use the same thread for multiple requests            
                Bind<ISession>().ToMethod(x => SessionFactory.GetCurrentSession());
                Bind<IQuizService>().To<QuizService>().InRequestScope();
            }
        }
*/
    }
}
