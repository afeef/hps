﻿using System;
using System.IO;
using System.Web.Mvc;
using System.Web.Routing;
using Ideaware.Hps.Services.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Services.Stock;
using Ideaware.Hps.Services.Stock.Contracts;
using Ideaware.Hps.Services.Transactions;
using Ideaware.Hps.Services.Transactions.Contracts;
using NHibernate;
using NHibernate.Context;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Ideaware.Hps.Services.Security;
using Ideaware.Hps.Services.Security.Contracts;
using ISession = NHibernate.ISession;
using System.Web;

namespace Ideaware.Hps.Web.Mvc
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public enum AppEnvironment
    {
        Development,
        Test,
        Production
    }

    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        public MvcApplication()
        {
            this.BeginRequest += new EventHandler(MvcApplication_BeginRequest);
            this.EndRequest += new EventHandler(MvcApplication_EndRequest);
            this.AuthenticateRequest += new EventHandler(MvcApplication_AuthenticateRequest);
        }

        void MvcApplication_EndRequest(object sender, EventArgs e)
        {
            CurrentSessionContext.Unbind(SessionFactory).Dispose();
        }

        void MvcApplication_BeginRequest(object sender, EventArgs e)
        {
            CurrentSessionContext.Bind(SessionFactory.OpenSession());
        }

        void MvcApplication_AuthenticateRequest(object sender, EventArgs e)
        {
            //if (SiteConfiguration.Current.AuthorizationProviders.FormsAuth.IsDefined == true &&
            //    HttpContext.Current.User != null)
            //    Membership.GetUser(true);
        }


        public static AppEnvironment Environment
        {
            get
            {
#if DEBUG
                return AppEnvironment.Development;
#else
                return AppEnvironment.Production;
#endif
            }
        }

        public static ISessionFactory SessionFactory = CreateSessionFactory();

        private static ISessionFactory CreateSessionFactory()
        {
            var cfg = new NHibernate.Cfg.Configuration().Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "nhibernate.config"));
            cfg.SetProperty(NHibernate.Cfg.Environment.ConnectionStringName, System.Environment.MachineName);
            //NHibernateProfiler.Initialize();

            return cfg.BuildSessionFactory();
        }
    }
}