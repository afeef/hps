﻿using System.Web.Mvc;
using Ideaware.Hps.Web.Extensions;

namespace Ideaware.Hps.Web.Controllers.Filters
{
	/// <summary>
	/// Validates that FormsAuth / Membership is enabled. If the user is logged in, validates that the provider for the user is this.
	/// </summary>
	public class ValidateFormsAuthAttribute : BaseActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
		    if (!Config.AuthorizationProviders.FormsAuth.IsDefined)
		        filterContext.Result = ResultHelper.ForbiddenResult(filterContext.Controller);

		    base.OnActionExecuting(filterContext);
		}
	}
}
