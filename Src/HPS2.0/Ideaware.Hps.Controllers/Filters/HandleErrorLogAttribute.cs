﻿using System.Web.Mvc;
using Ideaware.Hps.Services;

namespace Ideaware.Hps.Web.Controllers.Filters
{
	public class HandleErrorLogAttribute : HandleErrorAttribute
	{
		public override void OnException(ExceptionContext filterContext)
		{
			if ((!filterContext.ExceptionHandled) && filterContext.HttpContext.IsCustomErrorEnabled)
			{
				//LoggerService.LogError(filterContext.Exception);
			}
			base.OnException(filterContext);
		}
	}
}
