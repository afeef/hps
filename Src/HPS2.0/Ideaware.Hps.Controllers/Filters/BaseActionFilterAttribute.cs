﻿using System.Web.Mvc;
using Ideaware.Hps.Common.Configuration;

namespace Ideaware.Hps.Web.Controllers.Filters
{
	/// <summary>
	/// Defines a base class containing common properties and methods used in the application
	/// </summary>
	public abstract class BaseActionFilterAttribute : ActionFilterAttribute
	{
		/// <summary>
		/// Gets or sets the current site configuration.
		/// </summary>
		/// <remarks>To ensure testability</remarks>
		public SiteConfiguration Config
		{
			get;
			set;
		}

		public BaseActionFilterAttribute()
		{
			Config = SiteConfiguration.Current;
		}
	}
}
