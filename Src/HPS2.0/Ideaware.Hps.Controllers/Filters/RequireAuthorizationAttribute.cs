﻿using System;
using System.Web;
using System.Web.Mvc;
using Ideaware.Hps.Entities.Enum;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Web.Extensions;
using System.Web.Routing;

namespace Ideaware.Hps.Web.Controllers.Filters
{
    public class RequireAuthorizationAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Required minimal UserGroup
        /// </summary>        
        public Permissions Permissions { get; set; }
        public RouteCollection Routes { get; set; }

        public RequireAuthorizationAttribute()
        {
            this.Routes = RouteTable.Routes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="permissions">Required minimal permissions</param>
        public RequireAuthorizationAttribute(Permissions permissions)
            : this()
        {
            this.Permissions = permissions;
        }

        /// <summary>
        /// Determines if the filter must return forbidden status in the case the user is not logged in
        /// </summary>
        public bool RefuseOnFail { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //var session = new SessionWrapper(httpContext.Session);
            
            return httpContext.User.Identity.IsAuthenticated;

            //return IsAuthorized(session.User);
        }

        /// <summary>
        /// Determines if a user is authorized
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected virtual bool IsAuthorized(User user)
        {
            return user != null;
        }

        /// <summary>
        /// Handles the request when the user is not authorized
        /// </summary>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (RefuseOnFail)
                filterContext.Result = ResultHelper.ForbiddenResult(filterContext.Controller);

            else
            {
                var redirectOnSuccess = filterContext.HttpContext.Request.Url.PathAndQuery;

                var path = this.Routes.GetVirtualPath(filterContext.RequestContext,
                    new RouteValueDictionary(new
                        {
                            controller = "Account",
                            action = "Login",
                            returnUrl = redirectOnSuccess,
                            permissions = this.Permissions
                        }));

                if (path == null)
                    throw new ArgumentException("Route for Account > Login not found.");

                var loginUrl = path.VirtualPath;

                filterContext.Result = new RedirectResult(loginUrl, false);
            }
        }
    }
}
