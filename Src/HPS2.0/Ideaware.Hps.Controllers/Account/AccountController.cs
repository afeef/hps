﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Ideaware.Hps.Common.Validation;
using Ideaware.Hps.Entities.Models;
using Ideaware.Hps.Services.Security.Contracts;
using Ideaware.Hps.Web.Extensions;
using Ideaware.Hps.Web.State;
using Ideaware.Hps.Web.ViewModels.Account;
using RegisterModel = Ideaware.Hps.Entities.Models.RegisterModel;

namespace Ideaware.Hps.Web.Controllers.Account
{
    public class AccountController : BaseController
    {        
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {            
            this._userService = userService;
        }

        #region Forms Authentication

        public ActionResult Login(string returnUrl)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl, string role)
        {
            if (!ModelState.IsValid)
                return View( model);

            var user = this._userService.SignIn(model.UserName, model.Password, model.RememberMe);
            
            if( user == null)            
            {
                this.FlashError("Incorrect username or password!");
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }            
            
            this.Session.User = user;

            if (!String.IsNullOrEmpty(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout(string returnUrl)
        {            
            this.Session.User = null;

            _userService.SignOut();

            return Redirect(returnUrl);
        }

        public ActionResult Register()
        {
            ViewData["PasswordLength"] = this._userService.MinRequiredPasswordLength;
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            ViewData["PasswordLength"] = this._userService.MinRequiredPasswordLength;

            if (ModelState.IsValid)
                try
                {
                    var createStatus = this._userService.CreateUser(model.UserName, model.Password, model.Email);

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        this._userService.SignIn(model.UserName, model.Password, false);

                        return RedirectToAction("Index", "Home");
                    }

                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
                catch (ValidationException ex)
                {
                    foreach (var error in ex.ValidationErrors)
                    {
                        this.ModelState.AddModelError(error.FieldName, error);
                    }
                }

            return View(model);
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            this.ViewBag.PasswordLength = this._userService.MinRequiredPasswordLength;

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            ViewBag.PasswordLength = this._userService.MinRequiredPasswordLength;

            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;

                try
                {
                    changePasswordSucceeded = this._userService.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                    return RedirectToAction("ChangePasswordSuccess");


                ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ResetPassword(string email)
        {
            try
            {
                var userName = this._userService.GetUserNameByEmail(email);

                ValidateRegistration(userName);
                //MembershipUser membershipUser = this._userService.GetUser(userName, true);

                //User user = _userService.GetByProviderId(AuthenticationProvider.Membership, membershipUser.ProviderUserKey.ToString());
                //string guid = System.Guid.NewGuid().ToString("N");//GUID without hyphens
                //UsersService.UpdatePasswordResetGuid(user.Id, guid, DateTime.Now.AddDays(2)); //Expire after 2 days. Maybe could be defined in config
                if (ModelState.IsValid)
                {
                    //string linkUrl = this.Domain + this.Url.RouteUrl(new
                    //{
                    //    controller = "FormsAuthentication",
                    //    action = "NewPassword",
                    //    guid = guid
                    //});
                    //NotificationsService.SendResetPassword(user, linkUrl);
                    return View("ResetPasswordEmailConfirmation");
                }
            }
            catch (ValidationException ex)
            {
                this.AddErrors(this.ModelState, ex);
            }
            return View();
        }

        public ActionResult NewPassword(string guid)
        {
            //Guid pwdResetGuid;
            //if (Guid.TryParseExact(guid, "N", out pwdResetGuid) == false)
            //{
            //    return ResultHelper.NotFoundResult(this);
            //}

            //User user = _userService.GetByPasswordResetGuid(AuthenticationProvider.Membership, pwdResetGuid.ToString("N"));
            //if (user == null || user.PasswordResetGuidExpireDate < DateTime.Now)
            //{
            //    return ResultHelper.ForbiddenResult(this);
            //}

            //MembershipUser membershipUser = Membership.GetUser(user.UserName);
            //if (membershipUser == null)
            //{
            //    return ResultHelper.ForbiddenResult(this);
            //}

            //MembershipProvider provider = Membership.Provider;
            //FormsAuth.SignIn(membershipUser.UserName, false);
            //SecurityHelper.TryFinishMembershipLogin(base.Session, membershipUser);
            //Session.IsPasswordReset = true;

            return RedirectToAction("ChangePassword");
        }

        #endregion

        #region Status Codes

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        #endregion

        #region Validation Methods
        /// <summary>
        /// Validates change password form params
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        //[NonAction]
        //private void ValidateChangePassword(string currentPassword, string newPassword, string confirmPassword)
        //{
        //    var errors = new List<ValidationError>();
        //    if ((!Session.IsPasswordReset) && String.IsNullOrEmpty(currentPassword))
        //    {
        //        errors.Add(new ValidationError("currentPassword", ValidationErrorType.NullOrEmpty));
        //    }
        //    if (newPassword == null || newPassword.Length < MembershipService.MinPasswordLength)
        //    {
        //        errors.Add(new ValidationError("newPassword", ValidationErrorType.NullOrEmpty));
        //    }
        //    else if (newPassword != confirmPassword)
        //    {
        //        errors.Add(new ValidationError("newPassword", ValidationErrorType.CompareNotMatch));
        //    }

        //    if (errors.Count > 0)
        //    {
        //        throw new ValidationException(errors);
        //    }
        //}


        //[NonAction]
        //private bool ValidateLogOn(string userName, string password)
        //{
        //    if (String.IsNullOrEmpty(userName))
        //    {
        //        ModelState.AddModelError("username", "You must specify a username.");
        //    }
        //    if (String.IsNullOrEmpty(password))
        //    {
        //        ModelState.AddModelError("password", "You must specify a password.");
        //    }
        //    if (!this._membershipService.ValidateUser(userName, password))
        //    {
        //        ModelState.AddModelError("_FORM", "The username or password provided is incorrect.");
        //    }

        //    return ModelState.IsValid;
        //}

        /// <summary>
        /// Maps MembershipCreateStatus values to model's ValidationException
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        protected void ValidateCreateStatus(MembershipCreateStatus createStatus)
        {
            switch (createStatus)
            {
                case MembershipCreateStatus.Success:
                    break;
                case MembershipCreateStatus.DuplicateUserName:
                    throw new ValidationException(new ValidationError("username", ValidationErrorType.DuplicateNotAllowed));
                case MembershipCreateStatus.DuplicateEmail:
                    throw new ValidationException(new ValidationError("email", ValidationErrorType.DuplicateNotAllowed));
                case MembershipCreateStatus.InvalidPassword:
                    throw new ValidationException(new ValidationError("password", ValidationErrorType.Format));
                case MembershipCreateStatus.InvalidEmail:
                    throw new ValidationException(new ValidationError("email", ValidationErrorType.Format));
                case MembershipCreateStatus.InvalidAnswer:
                    throw new ValidationException(new ValidationError("answer", ValidationErrorType.Format));
                case MembershipCreateStatus.InvalidQuestion:
                    throw new ValidationException(new ValidationError("question", ValidationErrorType.Format));
                case MembershipCreateStatus.InvalidUserName:
                    throw new ValidationException(new ValidationError("username", ValidationErrorType.Format));
                default:
                    throw new ValidationException(new ValidationError("__FORM", ValidationErrorType.Format));
            }
        }

        /// <summary>
        /// Validates fields that Membership does not.
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        //private void ValidateRegistration(string userName, string email, string password, string confirmPassword)
        //{
        //    if (!String.Equals(password, confirmPassword, StringComparison.Ordinal))
        //    {
        //        throw new ValidationException(new ValidationError("password", ValidationErrorType.CompareNotMatch));
        //    }
        //}

        private void ValidateRegistration(string userName)
        {
            if (userName == null)
            {
                throw new ValidationException(new ValidationError("email", ValidationErrorType.CompareNotMatch));
            }
        }

        #endregion
                                                              
    }
}