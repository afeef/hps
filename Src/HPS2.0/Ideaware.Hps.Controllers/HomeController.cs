﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Enum;
using Ideaware.Hps.Web.Controllers.Filters;

namespace Ideaware.Hps.Web.Controllers
{
    //[RequireAuthorization(Role = "Admin")]
    [Permissions(Permissions.View)]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to HSTrader.net!";

            if (this.Request.IsAuthenticated)
            {
                return View();    
            }

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
