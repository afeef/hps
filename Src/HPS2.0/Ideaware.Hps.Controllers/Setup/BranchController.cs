﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Setup
{
    [RequireAuthorization]
    public class BranchController : BaseController
    {
        private readonly IBranchService _branchService;

        public BranchController(IBranchService branchService)
        {
            this._branchService = branchService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._branchService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.BranchName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }
       
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Branch branch)
        {

            UpdateModel(branch);

            if (ModelState.IsValid)
            {
                try
                {                    

                    this._branchService.Save(branch);

                    this.FlashInfo("Branch saved...");
                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }


        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._branchService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Branch branch)
        {                        
            this.TryUpdateModel(branch);

            if (ModelState.IsValid)
            {
                try
                {                    
                    this._branchService.Update(branch);
                    this.FlashInfo("Branch updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {
         
            return View(this._branchService.FindBy(id));
        }       

        public ActionResult Delete(int id)
        {            
            return View(this._branchService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._branchService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
