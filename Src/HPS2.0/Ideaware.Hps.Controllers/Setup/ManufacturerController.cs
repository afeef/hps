﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Setup
{
    [RequireAuthorization]
    public class ManufacturerController : BaseController
    {
        private readonly IManufacturerService _manufacturerService;

        public ManufacturerController(IManufacturerService manufacturerService)
        {
            this._manufacturerService = manufacturerService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._manufacturerService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.ManufacturerName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        //
        // GET: /CourseList/Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Manufacturer manufacturer)
        {
            UpdateModel(manufacturer);

            if (ModelState.IsValid)
            {
                try
                {
                    this._manufacturerService.Save(manufacturer);

                    this.FlashInfo("Manufacturer saved...");
                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }


        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._manufacturerService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Manufacturer manufacturer)
        {
            this.TryUpdateModel(manufacturer);

            if (ModelState.IsValid)
            {
                try
                {
                    this._manufacturerService.Update(manufacturer);
                    this.FlashInfo("Manufacturer updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {

            return View(this._manufacturerService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._manufacturerService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._manufacturerService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
