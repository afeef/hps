﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Setup
{    
    public class ColorController : BaseController
    {
        private readonly IColorService _colorService;

        public ColorController(IColorService colorService)
        {
            this._colorService = colorService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {            
            var colors = this._colorService.FindAll(gridSortOptions, searchWord, page, pageSize);

            colors.ViewData = ViewData;
            colors.AddFilter("searchWord", searchWord, a => a.ColorName.Contains(searchWord));
            colors.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", colors);

            return View(colors);
        }

        //
        // GET: /CourseList/Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Color color)
        {
            UpdateModel(color);

            if (ModelState.IsValid)
            {
                try
                {                    
                    this._colorService.Save(color);

                    this.FlashInfo("Color saved...");
                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._colorService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Color color)
        {
            this.TryUpdateModel(color);

            if (ModelState.IsValid)
            {
                try
                {
                    color.AccessUserId = this.Session.User.Key;
                    this._colorService.Update(color);
                    this.FlashInfo("Color updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {

            return View(this._colorService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._colorService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._colorService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
