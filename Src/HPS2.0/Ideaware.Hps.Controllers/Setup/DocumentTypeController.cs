﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Setup
{
   [RequireAuthorization]
    public class DocumentTypeController : BaseController
    {
        private readonly IDocumentTypeService _documentTypeService;

        public DocumentTypeController(IDocumentTypeService documentTypeService)
        {
            this._documentTypeService = documentTypeService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._documentTypeService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.DocumentName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        //
        // GET: /CourseList/Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(DocumentType documentType)
        {
            UpdateModel(documentType);

            if (ModelState.IsValid)
            {
                try
                {                    
                    this._documentTypeService.Save(documentType);

                    this.FlashInfo("Document type saved...");
                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        } 


        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._documentTypeService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, DocumentType documentType)
        {
            this.TryUpdateModel(documentType);

            if (ModelState.IsValid)
            {
                try
                {
                    this._documentTypeService.Update(documentType);
                    this.FlashInfo("DocumentType updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {

            return View(this._documentTypeService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._documentTypeService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._documentTypeService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
