﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Setup
{
    public class TenantController: BaseController
    {
        private readonly ITenantService _tenantService;

        public TenantController(ITenantService tenantService)
        {
            this._tenantService = tenantService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var colors = this._tenantService.FindAll(gridSortOptions, searchWord, page, pageSize);

            colors.ViewData = ViewData;
            colors.AddFilter("searchWord", searchWord, a => a.TenantName.Contains(searchWord));
            colors.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", colors);

            return View(colors);
        }

        //
        // GET: /CourseList/Create

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Tenant tenant)
        {
            UpdateModel(tenant);

            if (ModelState.IsValid)
            {
                try
                {
                    this._tenantService.Save(tenant);

                    this.FlashInfo("Color saved...");
                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }

        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            return View(this._tenantService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Tenant tenant)
        {
            this.TryUpdateModel(tenant);

            if (ModelState.IsValid)
            {
                try
                {
                    tenant.AccessUserId = this.Session.User.Key;
                    this._tenantService.Update(tenant);
                    this.FlashInfo("Color updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {

            return View(this._tenantService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._tenantService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._tenantService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
