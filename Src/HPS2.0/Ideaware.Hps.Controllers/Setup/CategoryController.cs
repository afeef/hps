﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Setup
{
    [RequireAuthorization]
    public class CategoryController: BaseController
    {
        private readonly ICategoryService _categoryService;        

        public CategoryController(ICategoryService categoryService)
        {
            this._categoryService = categoryService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var colors = this._categoryService.FindAll(gridSortOptions, searchWord, page, pageSize);

            colors.ViewData = ViewData;
            colors.AddFilter("searchWord", searchWord, a => a.CategoryName.Contains(searchWord));
            colors.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", colors);

            return View(colors);
        }               

        public ActionResult Create()
        {            
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Category category)
        {            
            this.TryUpdateModel(category);

            if (ModelState.IsValid)
            {
                try
                {                                        
                    this._categoryService.Save(category);

                    this.FlashInfo("category saved...");
                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }
            return this.Create();
        }
        
        public ActionResult Edit(int id)
        {            
            return View(this._categoryService.FindBy(id));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, Category category)
        {
            this.TryUpdateModel(category);

            if (ModelState.IsValid)
            {
                try
                {                    
                    this._categoryService.Update(category);
                    this.FlashInfo("Category updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {
         
            return View(this._categoryService.FindBy(id));
        }       

        public ActionResult Delete(int id)
        {            
            return View(this._categoryService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._categoryService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
