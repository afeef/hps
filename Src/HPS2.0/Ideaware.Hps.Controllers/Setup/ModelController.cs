﻿using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using Ideaware.Hps.Web.ViewModels.Setup;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Setup
{
    [RequireAuthorization]
    public class ModelController : BaseController
    {
        private readonly IModelService _modelService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;

        public ModelController(IModelService modelService, ICategoryService categoryService, IManufacturerService manufacturerService)
        {
            this._modelService = modelService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._modelService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.ModelName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        //
        // GET: /CourseList/Create

        public ActionResult Create()
        {
            var catagories = this._categoryService.FindAll();
            var manufacturers = this._manufacturerService.FindAll();
            return View(new ModelViewModel(new Model(), catagories, manufacturers));
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(ModelViewModel modelViewModel)
        {
            var model = modelViewModel.Model;

            this.TryUpdateModel(model);

            if (ModelState.IsValid)
            {
                try
                {
                    var category = this._categoryService.FindBy(modelViewModel.CategoryId);
                    var manufacturer = this._manufacturerService.FindBy(modelViewModel.ManufacturerId);

                    model.Category = category;
                    model.Manufacturer = manufacturer;

                    this._modelService.Save(model);

                    this.FlashInfo("Model saved...");
                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Create();
                }
            }

            return this.Create();
        }


        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id)
        {
            var catagories = this._categoryService.FindAll();
            var manufacturers = this._manufacturerService.FindAll();
            
            return View(new ModelViewModel(this._modelService.FindBy(id), catagories, manufacturers));            
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, ModelViewModel modelViewModel)
        {
            var model = modelViewModel.Model;

            this.TryUpdateModel(model);

            if (ModelState.IsValid)
            {
                try
                {
                    var category = this._categoryService.FindBy(modelViewModel.CategoryId);
                    var manufacturer = this._manufacturerService.FindBy(modelViewModel.ManufacturerId);

                    model.Category = category;
                    model.Manufacturer = manufacturer;

                    this._modelService.Update(model);
                    this.FlashInfo("Model updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult Details(int id)
        {

            return View(this._modelService.FindBy(id));
        }

        public ActionResult Delete(int id)
        {
            return View(this._modelService.FindBy(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._modelService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
