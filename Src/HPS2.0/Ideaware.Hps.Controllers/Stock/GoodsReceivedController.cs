﻿using System;
using System.Web.Mvc;
using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Services.Stock.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using Ideaware.Hps.Web.ViewModels.Stock;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Stock
{
    [RequireAuthorization]
    public class GoodsReceivedController : BaseController
    {
        private readonly IGoodsReceivedService _goodsReceivedService;
        private readonly IProductService _productService;
        private readonly IVendorService _vendorService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IModelService _modelService;
        private readonly IBranchService _branchService;
        private readonly IColorService _colorService;


        public GoodsReceivedController(IGoodsReceivedService goodsReceivedService,
            IProductService productService,
            IVendorService vendorService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IModelService modelService,
            IBranchService branchService,
            IColorService colorService)
        {
            this._goodsReceivedService = goodsReceivedService;
            this._productService = productService;
            this._vendorService = vendorService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._modelService = modelService;
            this._branchService = branchService;
            this._colorService = colorService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._goodsReceivedService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.GoodsReceivedNo.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create()
        {
            var categories = this._categoryService.FindAll();
            var vendors = this._vendorService.FindAll();

            var model = new GoodsReceivedViewModel(new GoodsReceived(), categories, vendors);

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(GoodsReceivedViewModel viewModel)
        {
            var goodsReceived = viewModel.GoodsReceived;

            goodsReceived.Category = this._categoryService.FindBy(viewModel.CategoryId);
            goodsReceived.Vendor = this._vendorService.FindBy(viewModel.VendorId);

            this.TryUpdateModel(goodsReceived);

            try
            {
                if (ModelState.IsValid)
                {
                    this._goodsReceivedService.Save(goodsReceived);                    
                    return RedirectToAction("AddProducts", new { goodsReceivedId = goodsReceived.Key });
                }
            }
            catch (Exception)
            {
                this.FlashError("Error occured while saving goods received");
                return this.Create();
            }

            return RedirectToAction("AddProducts");
        }

        public ActionResult Edit(int id)
        {
            var categories = this._categoryService.FindAll();
            var vendors = this._vendorService.FindAll();
            var goodsReceived = this._goodsReceivedService.FindBy(id);
            var model = new GoodsReceivedViewModel(goodsReceived, categories, vendors);

            return View(model);            
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles="Administrator")]
        public ActionResult Edit(int id, GoodsReceivedViewModel viewModel)
        {
            var goodsReceived = viewModel.GoodsReceived;

            this.TryUpdateModel(goodsReceived);

            if (ModelState.IsValid)
            {
                try
                {
                    var category = this._categoryService.FindBy(viewModel.CategoryId);
                    var vendor = this._vendorService.FindBy(viewModel.VendorId);

                    goodsReceived.Category = category;
                    goodsReceived.Vendor = vendor;

                    this._goodsReceivedService.Update(goodsReceived);
                    this.FlashInfo("Goods received updated successfully!");

                    return RedirectToAction("Index");
                }
                catch
                {
                    this.FlashError("There was an error saving this record");
                    return this.Edit(id);
                }
            }
            return this.Edit(id);
        }

        public ActionResult AddProducts(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10, int id = 0)
        {            
            var items = this._productService.FindByGoodReceived(gridSortOptions, searchWord, page, pageSize, id);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.ProductName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_gridProducts", items);

            var manufacturers = this._manufacturerService.FindAll();
            var models = this._modelService.FindAll();
            var branches = this._branchService.FindAll();
            var colors = this._colorService.FindAll();

            var productsModel = new AddProductsViewModel(id, items, manufacturers, models, branches, colors);

            return View(productsModel);
        }

        [HttpPost]
        public ActionResult AddProducts(AddProductsViewModel viewModel)
        {
            var manufacturer = this._manufacturerService.FindBy(viewModel.ManufacturerId);
            var model = this._modelService.FindBy(viewModel.ModelId);
            var branch = this._branchService.FindBy(viewModel.BranchId);
            var color = this._colorService.FindBy(viewModel.ColorId);
            var goodsReceived = this._goodsReceivedService.FindBy(viewModel.GoodsReceivedId);

            var product = viewModel.Product;

            product.Manufacturer = manufacturer;
            product.Model = model;
            product.Branch = branch;
            product.Color = color;
            product.GoodsReceived = goodsReceived;

            this.TryUpdateModel(product);

            try
            {
                if (this.ModelState.IsValid)
                {
                    this._productService.Save(product);
                    this.FlashInfo("Product saved successfully!");
                }
                else
                    this.FlashError("Error occured while saving product!");

                return RedirectToAction("AddProducts", new { id = viewModel.GoodsReceivedId });
            }
            catch (Exception)
            {
                this.FlashError("Error occured while saving product!");
                return View();
            }
        }        

        public ActionResult EditProducts(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10, int id = 0)
        {
            var items = this._productService.FindByGoodReceived(gridSortOptions, searchWord, page, pageSize, id);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.ProductName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_gridProducts", items);

            return View(items);
        }
    }
}
