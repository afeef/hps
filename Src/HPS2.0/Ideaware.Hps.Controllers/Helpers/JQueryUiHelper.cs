﻿namespace Ideaware.Hps.Web.Controllers.Helpers
{     
   public static class JQueryUiHelper
    {
        public static string InsertIcon(string iconName) 
        {
            return string.Format("<span class='ui-icon ui-{0}' style='float: left; margin-right: .2em;\'></span>"
                ,iconName);            
        }
    }
}
