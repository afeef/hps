﻿using System.Web.Mvc;
using Ideaware.Hps.Services;

namespace Ideaware.Hps.Web.Controllers
{
    public class AjaxController : BaseController
    {
        public ActionResult Index()
        {
            ViewData["Message"] = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult TabExample()
        {
            var serv = new TabTextService();
            var myModel = serv.GetTabViewModel();

            return View(myModel);
        }

        public ActionResult TabAjaxExample()
        {
            return View();
        }

        public ActionResult GetAjaxTab(int id)
        {
            string viewName = string.Empty;

            var serv = new TabTextService();
            var myModel = serv.GetTabViewModel();

            switch (id)
            {
                case 1:
                    viewName = "_tab1";
                    break;
                case 2:
                    viewName = "_tab2";
                    break;
                case 3:
                    viewName = "_tab3";
                    break;
                case 4:
                    viewName = "_error";
                    break;
            }

            System.Threading.Thread.Sleep(1000);

            return PartialView(viewName, myModel);
        }

    }
}
