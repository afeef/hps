﻿using System.Web.Mvc;
using Ideaware.Hps.Services.Setup.Contracts;

namespace Ideaware.Hps.Web.Controllers
{
    public class MapController: BaseController
    {
        private readonly IBranchService _branchService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IVendorService _vendorService;

        public MapController(IBranchService branchService,
            IManufacturerService manufacturerService,
            IVendorService vendorService)
        {
            this._branchService = branchService;
            this._manufacturerService = manufacturerService;
            this._vendorService = vendorService;
        }

        public ActionResult Index()
        {           
            return View();
        }
    }
}
