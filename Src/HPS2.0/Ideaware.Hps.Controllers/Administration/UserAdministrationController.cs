﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Services.Security.Contracts;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using Ideaware.Hps.Web.ViewModels.Administration;
using System.Net.Mail;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Administration
{
    [RequireAuthorization] // allows access if you're the only user, only validates role if role provider is enabled
    public class UserAdministrationController : BaseController
    {
        private const int PageSize = 10;
        private const string ResetPasswordBody = "Your new password is: ";
        private const string ResetPasswordSubject = "Your New Password";
        private readonly IRoleService _roleService;
        private readonly ISmtpClientService _smtpClient;
        private readonly IUserService _userService;
        private readonly IBranchService _branchService;

        public UserAdministrationController(
            IUserService userService,
            IRoleService roleService,
            ISmtpClientService smtpClient,
            IBranchService branchService)
        {
            this._userService = userService;
            this._roleService = roleService;
            this._smtpClient = smtpClient;
            this._branchService = branchService;
        }

        public ActionResult Index(int? page, string search)
        {
            var startRowIndex = PageSize * (page ?? 1 - 1);

            var users = string.IsNullOrWhiteSpace(search)
                ? _userService.FindAll(startRowIndex, PageSize)
                : search.Contains("@")
                    ? _userService.FindByEmail(search, startRowIndex, PageSize)
                    : _userService.FindByUserName(search, startRowIndex, PageSize);

            if (!string.IsNullOrWhiteSpace(search) && users.Count == 1)
                return RedirectToAction("Details", new { id = users[0].Key });

            return View(new IndexViewModel
            {
                Search = search,
                Users = users,
                Roles = _roleService.FindAll(),
                IsRolesEnabled = true
            });
        }

        public ActionResult Index2(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._userService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = this.ViewData;
            items.AddFilter("searchWord", searchWord, a => a.UserName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult CreateRole(string id)
        {

            if (!string.IsNullOrWhiteSpace(id))
            {
                _roleService.Create(id);
            }

            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult DeleteRole(int id)
        {
            _roleService.Delete(id);
            return RedirectToAction("Index");
        }

        public ViewResult Role(int id)
        {
            return View(new RoleViewModel
            {
                Role = this._roleService.FindBy(id).LoweredRoleName,
                Users = _roleService.FindUserNamesByRole(id)
                                     .ToDictionary(
                                        k => k,
                                        v => _userService.FindBy(v)
                                     )
            });
        }

        public ViewResult Details(int id)
        {
            var user = _userService.FindBy(id);
            var userRoles = _roleService.Enabled
                ? _roleService.FindByUser(user)
                : Enumerable.Empty<string>();


            var detailsViewModel = new DetailsViewModel
                                       {
                                           CanResetPassword = this._userService.EnablePasswordRetrieval,
                                           RequirePasswordQuestionAnswerToResetPassword = this._userService.RequiresQuestionAndAnswer,
                                           DisplayName = user.UserName,
                                           User = user,
                                           Roles = _roleService.Enabled
                                                        ? _roleService.FindAll().ToDictionary(role => role, role => userRoles.Contains(role.LoweredRoleName))
                                                        : new Dictionary<Role, bool>(0),
                                           IsRolesEnabled = _roleService.Enabled,
                                           Status = user.IsOnline
                                                        ? DetailsViewModel.StatusEnum.Online
                                                        : !user.IsApproved
                                                              ? DetailsViewModel.StatusEnum.Unapproved
                                                              : user.IsLockedOut
                                                                    ? DetailsViewModel.StatusEnum.LockedOut
                                                                    : DetailsViewModel.StatusEnum.Offline
                                       };

            return View(detailsViewModel);
        }

        public ViewResult Password(int id)
        {
            var user = _userService.FindBy(id);
            var userRoles = _roleService.Enabled
                ? _roleService.FindByUser(user)
                : Enumerable.Empty<string>();
            return View(new DetailsViewModel
            {
                CanResetPassword = this._userService.EnablePasswordReset,
                RequirePasswordQuestionAnswerToResetPassword = this._userService.RequiresQuestionAndAnswer,
                DisplayName = user.UserName,
                User = user,
                Roles = _roleService.Enabled
                    ? _roleService.FindAll().ToDictionary(r => r, r => userRoles.Contains(r.LoweredRoleName))
                    : new Dictionary<Role, bool>(0),
                IsRolesEnabled = _roleService.Enabled,
                Status = user.IsOnline
                            ? DetailsViewModel.StatusEnum.Online
                            : !user.IsApproved
                                ? DetailsViewModel.StatusEnum.Unapproved
                                : user.IsLockedOut
                                    ? DetailsViewModel.StatusEnum.LockedOut
                                    : DetailsViewModel.StatusEnum.Offline
            });
        }

        public ViewResult UsersRoles(int id)
        {
            var user = _userService.FindBy(id);
            var userRoles = _roleService.FindByUser(user);
            return View(new DetailsViewModel
            {
                CanResetPassword = this._userService.EnablePasswordReset,
                RequirePasswordQuestionAnswerToResetPassword = this._userService.RequiresQuestionAndAnswer,
                DisplayName = user.UserName,
                User = user,
                Roles = _roleService.FindAll().ToDictionary(r => r, r => userRoles.Contains(r.LoweredRoleName)),
                IsRolesEnabled = true,
                Status = user.IsOnline
                            ? DetailsViewModel.StatusEnum.Online
                            : !user.IsApproved
                                ? DetailsViewModel.StatusEnum.Unapproved
                                : user.IsLockedOut
                                    ? DetailsViewModel.StatusEnum.LockedOut
                                    : DetailsViewModel.StatusEnum.Offline
            });
        }

        public ViewResult CreateUser()
        {
            var model = new CreateUserViewModel
            {
                InitialRoles = _roleService.FindAll().Select(r => r.LoweredRoleName).ToDictionary(k => k, v => false)
            };
            return View(model);
        }

        public ActionResult Create()
        {
            var branches = this._branchService.FindAll();
            var initialRoles = _roleService.FindAll().Select(r => r.LoweredRoleName).ToDictionary(k => k, v => false);
            var viewModel = new UserViewModel(new User(), branches, initialRoles);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Create(UserViewModel viewModel)
        {
            var user = viewModel.User;

            this.TryUpdateModel(user);

            if (!ModelState.IsValid)
                return View(viewModel);

            try
            {
                var branch = this._branchService.FindBy(viewModel.BranchId);

                user.Branch = branch;
                
                user.LastActivityDate = DateTime.Now;
                user.LastLockoutDate = DateTime.Now;
                user.LastLoginDate = DateTime.Now;
                user.LastPasswordChangedDate = DateTime.Now;

                this._userService.Save(user);

                if (viewModel.InitialRoles != null)
                {
                    var rolesToAddUserTo = viewModel.InitialRoles.Where(x => x.Value).Select(x => x.Key);

                    foreach (var role in rolesToAddUserTo)
                        _roleService.AddUserToRole(user, role);
                }

                this.FlashInfo("User saved...");
                return this.RedirectToAction("Index");
            }
            catch (Exception e)
            {
                this.FlashInfo("Error: " + e.Message);
                return View(viewModel);
            }            
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateUser(CreateUserViewModel createUserViewModel)
        {
            if (!ModelState.IsValid)
                return View(createUserViewModel);

            try
            {
                if (createUserViewModel.Password != createUserViewModel.ConfirmPassword)
                    throw new MembershipCreateUserException("Passwords do not match.");

                var user = _userService.Create(
                    createUserViewModel.Username,
                    createUserViewModel.Password,
                    createUserViewModel.Email,
                    createUserViewModel.PasswordQuestion,
                    createUserViewModel.PasswordAnswer,
                    true);

                if (createUserViewModel.InitialRoles != null)
                {
                    var rolesToAddUserTo = createUserViewModel.InitialRoles.Where(x => x.Value).Select(x => x.Key);
                    foreach (var role in rolesToAddUserTo)
                        _roleService.AddUserToRole(user, role);
                }

                return RedirectToAction("Details", new { id = user.Key });
            }
            catch (MembershipCreateUserException e)
            {
                ModelState.AddModelError(string.Empty, e.Message);
                return View(createUserViewModel);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult Details(int id, string email)
        {
            var user = _userService.FindBy(id);
            user.Email = email;
            user.IsApproved = true;

            _userService.Update(user);
            return RedirectToAction("Details", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult DeleteUser(int id)
        {
            var user = _userService.FindBy(id);

            if (_roleService.Enabled)
                _roleService.RemoveFromAllRoles(user);

            _userService.Delete(user.Key);

            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult ChangeApproval(int id, bool isApproved)
        {
            var user = _userService.FindBy(id);
            user.IsApproved = isApproved;

            _userService.Update(user);

            return RedirectToAction("Details", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult Unlock(int id)
        {
            this._userService.Unlock(id);

            return RedirectToAction("Details", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult ResetPassword(int id)
        {
            var user = _userService.FindBy(id);
            var newPassword = this._userService.ResetPassword(user.UserName, user.Password);

            var body = ResetPasswordBody + newPassword;
            var msg = new MailMessage();
            msg.To.Add(user.Email);
            msg.Subject = ResetPasswordSubject;
            msg.Body = body;
            _smtpClient.Send(msg);

            return RedirectToAction("Password", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult ResetPasswordWithAnswer(int id, string answer)
        {
            var user = _userService.FindBy(id);
            var newPassword = this._userService.ResetPassword(user.UserName, answer);

            var body = ResetPasswordBody + newPassword;
            var msg = new MailMessage();
            msg.To.Add(user.Email);
            msg.Subject = ResetPasswordSubject;
            msg.Body = body;
            _smtpClient.Send(msg);

            return RedirectToAction("Password", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult SetPassword(int id, string password)
        {
            var user = _userService.FindBy(id);
            this._userService.ChangePassword(user.UserName, password);

            var body = ResetPasswordBody + password;
            var msg = new MailMessage();
            msg.To.Add(user.Email);
            msg.Subject = ResetPasswordSubject;
            msg.Body = body;
            _smtpClient.Send(msg);

            return RedirectToAction("Password", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult AddToRole(int id, string role)
        {
            _roleService.AddUserToRole(_userService.FindBy(id), role);

            return RedirectToAction("UsersRoles", new { id });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public RedirectToRouteResult RemoveFromRole(int id, string role)
        {
            _roleService.RemoveFromRole(_userService.FindBy(id), role);
            return RedirectToAction("UsersRoles", new { id });
        }
    }
}
