﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Entities.Transactions;
using Ideaware.Hps.Services.Security.Contracts;
using Ideaware.Hps.Services.Setup.Contracts;
using Ideaware.Hps.Services.Stock.Contracts;
using Ideaware.Hps.Services.Transactions.Contracts;
using Ideaware.Hps.Web.Controllers.Filters;
using Ideaware.Hps.Web.Extensions;
using Ideaware.Hps.Web.ViewModels.Transactions;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.Controllers.Transactions
{
    [RequireAuthorization]
    public class BookingController : BaseController
    {
        private readonly IBookingService _bookingService;
        private readonly IInstallmentPlanService _installmentPlanService;
        private readonly IBranchService _branchService;
        private readonly IUserService _userService;
        private readonly IProductService _productService;

        private const string Keys = "keys";

        public BookingController(IBookingService bookingService,
            IBranchService branchService,
            IUserService userService,
            IProductService productService,
            IInstallmentPlanService installmentPlanService)
        {
            this._bookingService = bookingService;
            this._branchService = branchService;
            this._userService = userService;
            this._productService = productService;
            this._installmentPlanService = installmentPlanService;
        }

        public ActionResult Index(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._bookingService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.AccountNo.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_grid", items);

            return View(items);
        }

        public ActionResult Create(int bookingId = 0)
        {

            if (bookingId == 0)
            {
                var bookingViewModel = new BookingViewModel(new Booking(), new Product(), null, null);
                return View(bookingViewModel);
            }
            else
            {
                var booking = this._bookingService.FindBy(bookingId);
                var installmentPlan = this._installmentPlanService.FindByBooking(bookingId);
                var bookingViewModel = new BookingViewModel(booking, booking.Product, booking.Guarantors,
                                                            installmentPlan);

                return View(bookingViewModel);
            }
        }

        [HttpPost]
        public ActionResult Create(BookingViewModel model)
        {
            var booking = model.Booking;

            booking.Product = this._productService.FindBy(model.ProductId);
            booking.Customer = this._userService.FindBy(model.CustomerId);
            booking.Branch = this._branchService.FindBy(model.BranchId);

            var keys = model.GuarantorKeys.Split(',');

            booking.Guarantors = new List<User>(keys.Select(key => this._userService.FindBy(Convert.ToInt32(key))));

            this.TryUpdateModel(booking);

            try
            {
                if (ModelState.IsValid)
                {
                    this._bookingService.Save(booking);
                    this.GenerateInstallmentPlan(booking);
                    this.FlashInfo("Booking saved successfully!");
                }
            }
            catch
            {
                this.FlashError("Error occured while saving booking!");
            }

            return this.Create(booking.Key);
        }

        private void GenerateInstallmentPlan(Booking booking)
        {
            var installmentAmount = booking.SalePrice / booking.NoOfInstallments;

            for (var i = 1; i <= booking.NoOfInstallments; i++)
            {
                var installmentPlan = new InstallmentPlan
                                          {
                                              InstallmentNo = i,
                                              InstallmentDate = DateTime.Today.AddMonths(i),
                                              InstallmentAmount = installmentAmount,
                                              ProfitAmount = installmentAmount,
                                              IsComplete = false,
                                              Booking = booking
                                          };

                this._installmentPlanService.Save(installmentPlan);
            }
        }

        [HttpPost]
        public ActionResult GetSelectedGuarantors(List<string> keys)
        {
            var guarantors = keys.Select(key => this._userService.FindBy(Convert.ToInt32(key))).ToList();

            if (Request.IsAjaxRequest())
                return PartialView("_selectedGuarantors", guarantors);

            return View("Index");
        }

        public ActionResult GetBranches(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._branchService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.BranchName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_branches", items);

            return RedirectToAction("Index");
        }

        public ActionResult GetCustomers(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._userService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.UserName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_customers", items);

            return RedirectToAction("Index");
        }

        public ActionResult GetGuarantors(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._userService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.UserName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_guarantors", items);

            return RedirectToAction("Index");
        }

        public ActionResult GetProducts(GridSortOptions gridSortOptions, string searchWord, int page = 1, int pageSize = 10)
        {
            var items = this._productService.FindAll(gridSortOptions, searchWord, page, pageSize);

            items.ViewData = ViewData;
            items.AddFilter("searchWord", searchWord, a => a.ProductName.Contains(searchWord));
            items.Setup();

            if (Request.IsAjaxRequest())
                return PartialView("_products", items);

            return RedirectToAction("Index");
        }
    }
}
