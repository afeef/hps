﻿using System.Web.Optimization;

namespace Ideaworx.Hps.Web.Mvc.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/public/jquery/jquery-1.*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/public/jquery.ui.themes/jquery-ui*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/public/jquery.plugins/jquery.unobtrusive*",
                        "~/public/jquery.plugins/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/public/modernizr/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/public/jquery/jquery-1.*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include("~/public/bootstrap/js/bootstrap.js"));


            bundles.Add(new StyleBundle("~/content/css")
                .Include("~/public/jquery.ui.themes/flick/jquery-ui-1.8.18.custom.css"));
        }
    }
}