﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using Ideaware.Hps.Common;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Services.Security.Contracts;
using NHibernate;
using NHibernate.Linq;

namespace Ideaware.Hps.Services.Security
{
    public class UserService : BaseService<User>, IUserService
    {

        #region Properties

        public int MaxInvalidPasswordAttempts { get; private set; }
        public int MinRequiredNonAlphanumericCharacters { get; private set; }
        public int MinRequiredPasswordLength { get; private set; }
        public int PasswordAttemptWindow { get; private set; }
        public MembershipPasswordFormat PasswordFormat { get; private set; }
        public string PasswordStrengthRegularExpression { get; private set; }
        public bool RequiresQuestionAndAnswer { get; private set; }
        public bool RequiresUniqueEmail { get; private set; }
        public bool EnablePasswordReset { get; private set; }
        public bool EnablePasswordRetrieval { get; private set; }
        public string ServiceName { get { return "MembershipService"; } }

        #endregion

        public UserService(ISession session)
            : base(session)
        {
        }

        public User FindBy(string userName)
        {
            return (from u in this.Session.Query<User>()
                    where u.UserName == userName
                    select u).SingleOrDefault();
        }

        public User FindBy(string userName, string password)
        {
            var user = this.Session.Query<User>()
                           .FetchMany(u => u.Roles)
                           .ThenFetchMany(r => r.Permissions)
                           .SingleOrDefault(u => u.UserName == userName);

            return user;

            //return (from u in this.Session.Query<User>()
            //        where u.UserName == userName
            //              && u.Password == password
            //        select u).SingleOrDefault();
        }

        public IList<User> FindByUserName(string search, int startRowIndex, int pageSize)
        {
            return (from u in this.Session.Query<User>()
                    where u.UserName == search
                    select u)
                    .Skip(startRowIndex)
                    .Take(pageSize)
                    .ToList();
        }

        public User FindByEmail(string email)
        {
            return (from u in this.Session.Query<User>()
                    where u.Email == email
                    select u).SingleOrDefault();
        }

        public IList<User> FindByEmail(string search, int startRowIndex, int pageSize)
        {
            return (from u in this.Session.Query<User>()
                    where u.Email == search
                    select u)
                    .Skip(startRowIndex)
                    .Take(pageSize)
                    .ToList();
        }

        #region MembershipService Implementation

        public bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            if (newPassword.Length < this.MinRequiredPasswordLength)
                throw new ArgumentException("Password is too short!");

            //var nonAlphanumericCharactersCount = newPassword.Where((t, cnt) => !char.IsLetterOrDigit(newPassword, cnt)).Count();

            //if (nonAlphanumericCharactersCount < MinRequiredNonAlphanumericCharacters)
            //    throw new ArgumentException("Password requires more non aplphanumeric characters");

            //if ((PasswordStrengthRegularExpression.Length > 0) && !Regex.IsMatch(newPassword, PasswordStrengthRegularExpression))
            //    throw new ArgumentException("Password does not match regular expression");

            var user = this.FindBy(username, oldPassword);

            if (user == null)
                return false;

            user.Password = newPassword;
            this.Update(user);

            return true;
        }

        public bool ChangePasswordQuestionAndAnswer(string username, string password, string passwordQuestion, string passwordAnswer)
        {
            if (string.IsNullOrEmpty(passwordAnswer))
                throw new ArgumentException("New Password cannot be null or empty");

            var user = this.FindBy(username, password);

            if (user == null)
                return false;

            user.PasswordQuestion = passwordQuestion;
            user.PasswordAnswer = passwordAnswer;

            this.Update(user);

            return true;
        }

        public MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isActive, object providerUserKey, out MembershipCreateStatus status)
        {

            if ((providerUserKey != null) && !(providerUserKey is int))
            {
                status = MembershipCreateStatus.InvalidProviderUserKey;
                return null;
            }

            if (password.Length < MinRequiredPasswordLength)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            var nonAlphanumericCharactersCount = password.Where((t, cnt) => !char.IsLetterOrDigit(password, cnt)).Count();

            if (nonAlphanumericCharactersCount < MinRequiredNonAlphanumericCharacters)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            //if ((PasswordStrengthRegularExpression.Length > 0) &&
            //!Regex.IsMatch(password, PasswordStrengthRegularExpression))
            //{
            //    status = MembershipCreateStatus.InvalidPassword;
            //    return null;
            //}            

            var user = new User()
            {
                UserName = username,
                Password = password,
                Email = email,
                PasswordQuestion = passwordQuestion,
                PasswordAnswer = passwordAnswer,
                IsActive = isActive
            };

            try
            {
                this.Save(user);
                status = MembershipCreateStatus.Success;
                providerUserKey = user.Key;
                var localTime = DateTime.UtcNow.ToLocalTime();

                return new MembershipUser(this.ServiceName, username, providerUserKey, email, passwordQuestion, null, isActive, false, localTime, localTime, localTime, localTime, localTime);
            }
            catch (Exception)
            {
                status = MembershipCreateStatus.UserRejected;
                return null;
            }
        }

        public bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            var user = this.FindBy(username);

            if (user == null)
                throw new Exception("username does not exist.");

            return this.Delete(user.Key);
        }

        public MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            if (pageIndex < 0)
                throw new ArgumentException("Page index cannot be negative!");
            if (pageSize < 1)
                throw new ArgumentException("Page size cannot be less than one!");

            var users = this.FindByEmail(emailToMatch, pageIndex, pageSize);

            totalRecords = this.Count;

            var collection = new MembershipUserCollection();

            foreach (var item in users)
            {
                var localTime = DateTime.UtcNow.ToLocalTime();
                collection.Add(new MembershipUser(this.ServiceName, item.UserName, item.Key, item.Email, item.PasswordQuestion, item.PasswordAnswer, item.IsActive, false, localTime, localTime, localTime, localTime, localTime));
            }

            return collection;
        }

        public MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            if (pageIndex < 0)
                throw new ArgumentException("Page index cannot be negative!");
            if (pageSize < 1)
                throw new ArgumentException("Page size cannot be less than one!");

            var users = this.FindByUserName(usernameToMatch, pageIndex, pageSize);
            totalRecords = this.Count;

            var collection = new MembershipUserCollection();

            foreach (var item in users)
            {
                var localTime = DateTime.UtcNow.ToLocalTime();
                collection.Add(new MembershipUser(this.ServiceName, item.UserName, item.Key, item.Email, item.PasswordQuestion, item.PasswordAnswer, item.IsActive, false, localTime, localTime, localTime, localTime, localTime));
            }

            return collection;
        }

        public MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            if (pageIndex < 0)
                throw new ArgumentException("Page index cannot be negative!");
            if (pageSize < 1)
                throw new ArgumentException("Page size cannot be less than one!");

            var users = this.FindAll(pageIndex, pageSize);
            totalRecords = this.Count;

            var collection = new MembershipUserCollection();

            foreach (var item in users)
            {
                var localTime = DateTime.UtcNow.ToLocalTime();
                collection.Add(new MembershipUser(this.ServiceName, item.UserName, item.Key, item.Email, item.PasswordQuestion, item.PasswordAnswer, item.IsActive, false, localTime, localTime, localTime, localTime, localTime));
            }

            return collection;
        }

        public int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public string GetPassword(string username, string passwordAnswer)
        {
            if (!EnablePasswordRetrieval)
                throw new NotSupportedException("Password cannot be retrieved!");

            if (string.IsNullOrEmpty(passwordAnswer))
                return null;

            var user = new User { UserName = username };

            if (RequiresQuestionAndAnswer)
                user.PasswordAnswer = passwordAnswer;

            user = this.FindBy(username);

            return user != null ? user.Password : null;
        }

        public MembershipUser GetUser(string username, bool userIsOnline)
        {
            var user = this.FindBy(username);

            if (user != null)
            {
                var localTime = DateTime.UtcNow.ToLocalTime();
                return new MembershipUser(this.ServiceName, user.UserName, user.Key, user.Email, user.PasswordQuestion, user.PasswordAnswer, DataHelper.ToBoolean(user.IsActive), false, localTime, localTime, localTime, localTime, localTime);
            }
            return null;
        }

        public MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {

            if (providerUserKey == null)
                throw new ArgumentNullException("providerUserKey cannot be null!");

            if (!(providerUserKey is int))
                throw new ArgumentException("providerUserKey is not of type int!");

            var user = this.FindBy(DataHelper.ToInteger(providerUserKey));

            if (user != null)
            {
                var localTime = DateTime.UtcNow.ToLocalTime();
                return new MembershipUser(this.ServiceName, user.UserName, user.Key, user.Email, user.PasswordQuestion, user.PasswordAnswer, user.IsActive, false, localTime, localTime, localTime, localTime, localTime);
            }

            return null;
        }

        public string GetUserNameByEmail(string email)
        {
            var user = this.FindByEmail(email);

            return user != null ? user.UserName : null;
        }

        public string ResetPassword(string username, string passwordAnswer)
        {
            if (!this.EnablePasswordReset)
                throw new NotSupportedException();

            if (this.RequiresQuestionAndAnswer && string.IsNullOrEmpty(passwordAnswer))
                return null;

            var user = this.FindBy(username, passwordAnswer);
            var generatedPassword = Membership.GeneratePassword(MinRequiredPasswordLength, MinRequiredNonAlphanumericCharacters);
            user.Password = generatedPassword;

            this.Update(user);

            return generatedPassword;
        }

        public bool UnlockUser(string userName)
        {
            var user = this.FindBy(userName);

            if (user == null)
                return false;

            user.IsActive = true;
            this.Update(user);

            return user.IsActive;
        }

        public void UpdateUser(MembershipUser membershipUser)
        {
            if (membershipUser == null)
                throw new ArgumentNullException();

            var user = new User()
            {
                UserName = membershipUser.UserName,
                Email = membershipUser.Email,
                PasswordQuestion = membershipUser.PasswordQuestion,
                PasswordAnswer = membershipUser.Comment,
                IsActive = membershipUser.IsApproved
            };

            this.Update(user);
        }

        public bool ValidateUser(string username, string password)
        {
            var user = this.FindBy(username, password);
            return user != null && user.IsActive;
        }

        public User Create(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved)
        {
            var user = new User()
            {
                UserName = username,
                Password = password,
                Email = email,
                PasswordQuestion = passwordQuestion,
                PasswordAnswer = passwordAnswer,
                IsActive = true,
                IsApproved = isApproved,
                LastActivityDate = DateTime.Now,
                LastLockoutDate = DateTime.Now,
                LastLoginDate = DateTime.Now,
                LastPasswordChangedDate = DateTime.Now
            };

            this.Save(user);

            return user;
        }

        public void Unlock(int id)
        {
            var user = this.FindBy(id);

            user.IsLockedOut = false;

            this.Save(user);
        }

        #endregion

        public MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            MembershipCreateStatus status;
            this.CreateUser(userName, password, email, null, null, true, null, out status);
            return status;
        }

        /// <summary>
        /// Changes the password of a user without asking the old password.
        /// </summary>
        public bool ChangePassword(string userName, string newPassword)
        {
            var currentUser = this.GetUser(userName, true);
            var oldPassword = currentUser.ResetPassword();
            return currentUser.ChangePassword(oldPassword, newPassword);
        }

        public User SignIn(string userName, string password, bool createPersistentCookie)
        {
            var user = this.FindBy(userName, password);
            
            if (user != null && user.IsActive)
            {
                FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
                return user;
            }

            return null;
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
