﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Linq;
using System.Web;
using System.Web.Security;
using Ideaware.Hps.Common;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Services.Security.Contracts;

namespace Ideaware.Hps.Services.Security
{
    public class MembershipService : MembershipProvider, IMembershipService
    {
        #region Private fields

        private static bool _isInitialized = false;
        private readonly string _connectionStringName = Environment.MachineName;
        private string _applicationName;
        private int _maxInvalidPasswordAttempts;
        private int _minRequiredNonAlphanumericCharacters;
        private int _minRequiredPasswordLength;
        private int _passwordAttemptWindow;
        private MembershipPasswordFormat _passwordFormat;
        private string _passwordStrengthRegularExpression;
        private bool _requiresQuestionAndAnswer;
        private bool _requiresUniqueEmail;
        private bool _enablePasswordReset;
        private bool _enablePasswordRetrieval;

        #endregion

        #region Properties

        public override string ApplicationName
        {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return _maxInvalidPasswordAttempts; }
            //set { _maxInvalidPasswordAttempts = value; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return _minRequiredNonAlphanumericCharacters; }
            //set { _minRequiredNonAlphanumericCharacters = value; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return _minRequiredPasswordLength; }
            //set { _minRequiredPasswordLength = value; }
        }

        public override int PasswordAttemptWindow
        {
            get { return _passwordAttemptWindow; }
            //set { _passwordAttemptWindow = value; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return _passwordFormat; }
            //set { _passwordFormat = value; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return _passwordStrengthRegularExpression; }
            //set { _passwordStrengthRegularExpression = value; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return _requiresQuestionAndAnswer; }
            //set { _requiresQuestionAndAnswer = value; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return _requiresUniqueEmail; }
            //set { _requiresUniqueEmail = value; }
        }

        public override bool EnablePasswordReset
        {
            get { return _enablePasswordReset; }
            //set { _enablePasswordReset = value; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return _enablePasswordRetrieval; }
            //set { _enablePasswordRetrieval = value; }
        }

        #endregion
       
        private readonly IUserService _userService;

        public MembershipService()
        {
        }

        public MembershipService(IUserService userService)
        {
            this._userService = userService;
        }

        public override void Initialize(string name, NameValueCollection config)
        {

            if (config == null)
                throw new ArgumentNullException("config");

            if (string.IsNullOrEmpty(name))
                name = "MembershipService";

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Stores membership information in SQL Server database");
            }

            base.Initialize(name, config);

            _enablePasswordRetrieval = (config["enablePasswordRetrieval"] != null) && bool.Parse(config["enablePasswordRetrieval"]);
            config.Remove("enablePasswordRetrieval");

            _enablePasswordReset = config["enablePasswordReset"] == null || bool.Parse(config["enablePasswordReset"]);
            config.Remove("enablePasswordReset");

            _requiresQuestionAndAnswer = config["requiresQuestionAndAnswer"] == null || bool.Parse(config["requiresQuestionAndAnswer"]);
            config.Remove("requiresQuestionAndAnswer");

            _requiresUniqueEmail = config["requiresUniqueEmail"] == null || bool.Parse(config["requiresUniqueEmail"]);
            config.Remove("requiresUniqueEmail");

            _maxInvalidPasswordAttempts = config["passwordAttemptWindow"] != null ? int.Parse(config["passwordAttemptWindow"]) : 5;
            config.Remove("maxInvalidPasswordAttempts");

            _passwordAttemptWindow = config["passwordAttemptWindow"] != null ? int.Parse(config["passwordAttemptWindow"]) : 10;
            config.Remove("passwordAttemptWindow");

            _minRequiredPasswordLength = config["minRequiredPasswordLength"] != null ? int.Parse(config["minRequiredPasswordLength"]) : 7;
            config.Remove("minRequiredPasswordLength");

            _minRequiredNonAlphanumericCharacters = config["minRequiredNonalphanumericCharacters"] != null ? int.Parse(config["minRequiredNonalphanumericCharacters"]) : 1;
            config.Remove("minRequiredNonalphanumericCharacters");

            _passwordStrengthRegularExpression = config["passwordStrengthRegularExpression"];
            config.Remove("passwordStrengthRegularExpression");

            if (_minRequiredNonAlphanumericCharacters > _minRequiredPasswordLength)
                throw new HttpException();

            _applicationName = config["applicationName"];

            if (string.IsNullOrEmpty(_applicationName))
                _applicationName = "/";

            config.Remove("applicationName");

            string strTemp = config["passwordFormat"];

            if (string.IsNullOrEmpty(strTemp))
                strTemp = "Hashed";

            switch (strTemp)
            {
                case "Clear":
                    _passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                case "Encrypted":
                    _passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Hashed":
                    _passwordFormat = MembershipPasswordFormat.Hashed;

                    break;
                default:
                    throw new ProviderException("Bad password format");
            }
            if ((PasswordFormat == MembershipPasswordFormat.Hashed) && EnablePasswordRetrieval)
                throw new ProviderException();

            config.Remove("passwordFormat");
            var settings = ConfigurationManager.ConnectionStrings[_connectionStringName];

            if (string.IsNullOrEmpty(settings.ConnectionString))
                throw new ProviderException("Invalid connection string name");

            config.Remove(_connectionStringName);

            if (config.Count > 0)
            {
                string key = config.GetKey(0);
                if (!string.IsNullOrEmpty(key))
                    throw new ProviderException("Unrecognized attribute");
            }
        }

        #region MembershipService Implementation

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            if (newPassword.Length < this.MinRequiredPasswordLength)
                throw new ArgumentException("Password is too short!");

            //var nonAlphanumericCharactersCount = newPassword.Where((t, cnt) => !char.IsLetterOrDigit(newPassword, cnt)).Count();

            //if (nonAlphanumericCharactersCount < MinRequiredNonAlphanumericCharacters)
            //    throw new ArgumentException("Password requires more non aplphanumeric characters");

            //if ((PasswordStrengthRegularExpression.Length > 0) && !Regex.IsMatch(newPassword, PasswordStrengthRegularExpression))
            //    throw new ArgumentException("Password does not match regular expression");

            var user = _userService.FindBy(username, oldPassword);

            if (user == null)
                return false;

            user.Password = newPassword;
            _userService.Update(user);

            return true;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string passwordQuestion, string passwordAnswer)
        {
            if (string.IsNullOrEmpty(passwordAnswer))
                throw new ArgumentException("New Password cannot be null or empty");

            var user = _userService.FindBy(username, password);

            if (user == null)
                return false;

            user.PasswordQuestion = passwordQuestion;
            user.PasswordAnswer = passwordAnswer;

            _userService.Update(user);

            return true;
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isActive, object providerUserKey, out MembershipCreateStatus status)
        {

            if ((providerUserKey != null) && !(providerUserKey is int))
            {
                status = MembershipCreateStatus.InvalidProviderUserKey;
                return null;
            }

            if (password.Length < MinRequiredPasswordLength)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            var nonAlphanumericCharactersCount = password.Where((t, cnt) => !char.IsLetterOrDigit(password, cnt)).Count();

            if (nonAlphanumericCharactersCount < MinRequiredNonAlphanumericCharacters)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            //if ((PasswordStrengthRegularExpression.Length > 0) &&
            //!Regex.IsMatch(password, PasswordStrengthRegularExpression))
            //{
            //    status = MembershipCreateStatus.InvalidPassword;
            //    return null;
            //}            

            var user = new User()
            {
                UserName = username,
                Password = password,
                Email = email,
                PasswordQuestion = passwordQuestion,
                PasswordAnswer = passwordAnswer,
                IsActive = isActive
            };

            try
            {
                _userService.Save(user);
                status = MembershipCreateStatus.Success;
                providerUserKey = user.Key;
                var localTime = DateTime.UtcNow.ToLocalTime();

                return new MembershipUser("MembershipService", username, providerUserKey, email, passwordQuestion, null, isActive, false, localTime, localTime, localTime, localTime, localTime);
            }
            catch (Exception)
            {
                status = MembershipCreateStatus.UserRejected;
                return null;
            }                                    
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            var user = _userService.FindBy(username);

            if (user == null)
                throw new Exception("username does not exist.");

            return _userService.Delete(user.Key);

        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {

            //var collection = new MembershipUserCollection();

            if (pageIndex < 0)
                throw new ArgumentException("Page index cannot be negative!");
            if (pageSize < 1)
                throw new ArgumentException("Page size cannot be less than one!");

            //var user = new User { Email = emailToMatch };

            //var users = _userService.FindAll(user, pageIndex, pageSize);
            //totalRecords = _userService.Count;

            //foreach (var item in users)
            //{
            //    var localTime = DateTime.UtcNow.ToLocalTime();
            //    collection.Add(new MembershipUser(Name, item.UserName, item.Key, item.Email, item.PasswordQuestion, item.PasswordAnswer, DataHelper.ToBoolean(user.IsActive), false, localTime, localTime, localTime, localTime, localTime));
            //}

            //return collection;
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {

            //var collection = new MembershipUserCollection();

            if (pageIndex < 0)
                throw new ArgumentException("Page index cannot be negative!");
            if (pageSize < 1)
                throw new ArgumentException("Page size cannot be less than one!");

            //var user = new User { UserName = usernameToMatch };

            //var users = _userService.FindAll(user, pageIndex, pageSize);
            //totalRecords = _userService.Count;

            //foreach (var item in users)
            //{
            //    var localTime = DateTime.UtcNow.ToLocalTime();
            //    collection.Add(new MembershipUser(Name, item.UserName, item.Key, item.Email, item.PasswordQuestion, item.PasswordAnswer, DataHelper.ToBoolean(user.IsActive), false, localTime, localTime, localTime, localTime, localTime));
            //}

            //return collection;
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {

            //var collection = new MembershipUserCollection();

            if (pageIndex < 0)
                throw new ArgumentException("Page index cannot be negative!");
            if (pageSize < 1)
                throw new ArgumentException("Page size cannot be less than one!");

            //var users = _userService.FindAll(new User(), pageIndex, pageSize);
            //totalRecords = _userService.Count;

            //foreach (var item in users)
            //{
            //    var localTime = DateTime.UtcNow.ToLocalTime();
            //    collection.Add(new MembershipUser(Name, item.UserName, item.Key, item.Email, item.PasswordQuestion, item.PasswordAnswer, DataHelper.ToBoolean(item.IsActive), false, localTime, localTime, localTime, localTime, localTime));
            //}

            //return collection;
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string passwordAnswer)
        {
            if (!EnablePasswordRetrieval)
                throw new NotSupportedException("Password cannot be retrieved!");

            if (string.IsNullOrEmpty(passwordAnswer))
                return null;

            var user = new User { UserName = username };

            if (RequiresQuestionAndAnswer)
                user.PasswordAnswer = passwordAnswer;

            user = _userService.FindBy(username);

            return user != null ? user.Password : null;
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            var user = _userService.FindBy(username);

            if (user != null)
            {
                var localTime = DateTime.UtcNow.ToLocalTime();
                return new MembershipUser("MembershipService", user.UserName, user.Key, user.Email, user.PasswordQuestion, user.PasswordAnswer, DataHelper.ToBoolean(user.IsActive), false, localTime, localTime, localTime, localTime, localTime);
            }
            return null;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {

            if (providerUserKey == null)
                throw new ArgumentNullException("providerUserKey cannot be null!");

            if (!(providerUserKey is int))
                throw new ArgumentException("providerUserKey is not of type int!");

            var user = _userService.FindBy(DataHelper.ToInteger(providerUserKey));

            if (user != null)
            {
                var localTime = DateTime.UtcNow.ToLocalTime();
                return new MembershipUser("MembershipService", user.UserName, user.Key, user.Email, user.PasswordQuestion, user.PasswordAnswer, user.IsActive, false, localTime, localTime, localTime, localTime, localTime);
            }

            return null;
        }

        public override string GetUserNameByEmail(string email)
        {            
            var user = _userService.FindByEmail(email);

            return user != null ? user.UserName : null;            
        }

        public override string ResetPassword(string username, string passwordAnswer)
        {
            //if (!this.EnablePasswordReset)
            //    throw new NotSupportedException();

            //var generatedPassword = Membership.GeneratePassword(MinRequiredPasswordLength, MinRequiredNonAlphanumericCharacters);
            //var user = new User { UserName = username };

            //if (RequiresQuestionAndAnswer && string.IsNullOrEmpty(passwordAnswer)) return null;

            //user.PasswordAnswer = passwordAnswer;
            //user = _userService.FindInstance(user);
            //user.Password = generatedPassword;
            //user.SetTimeStamp = Convert.FromBase64String(user.TimeStampVal);
            //_userService.Update(user);

            //return _userService.Commit() ? generatedPassword : null;
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            var user = _userService.FindBy(userName);

            if (user == null)
                return false;

            user.IsActive = true;
            _userService.Update(user);

            return user.IsActive;
        }

        public override void UpdateUser(MembershipUser membershipUser)
        {
            //if (membershipUser == null)
            //    throw new ArgumentNullException();

            //var user = new User(DataHelper.ToNullableGuid(membershipUser.ProviderUserKey))
            //{
            //    UserName = membershipUser.UserName,
            //    Email = membershipUser.Email,
            //    PasswordQuestion = membershipUser.PasswordQuestion,
            //    PasswordAnswer = membershipUser.Comment,
            //    IsActive = membershipUser.IsApproved
            //};

            //_userService.Update(user);
            //_userService.Commit();
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            var user = _userService.FindBy(username, password);
            return user != null && user.IsActive;
        }

        #endregion

        public MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            MembershipCreateStatus status;
            this.CreateUser(userName, password, email, null, null, true, null, out status);
            return status;
        }

        /// <summary>
        /// Changes the password of a user without asking the old password.
        /// </summary>
        public bool ChangePassword(string userName, string newPassword)
        {
            var currentUser = this.GetUser(userName, true);
            var oldPassword = currentUser.ResetPassword();
            return currentUser.ChangePassword(oldPassword, newPassword);
        }
    }
}
