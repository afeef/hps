﻿using System.Collections.Generic;
using System.Web.Security;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Services.Contracts;

namespace Ideaware.Hps.Services.Security.Contracts
{
    public interface IUserService: IService<User>
    {                        
        User FindBy(string userName);
        User FindBy(string userName, string password);
        IList<User> FindByUserName(string search, int i, int pageSize);               
        User FindByEmail(string email);
        IList<User> FindByEmail(string search, int i, int pageSize);
        MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords);
        MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
                                                 out int totalRecords);
        MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords);
        int GetNumberOfUsersOnline();
        string GetPassword(string username, string passwordAnswer);
        MembershipUser GetUser(string username, bool userIsOnline);
        MembershipUser GetUser(object providerUserKey, bool userIsOnline);
        string GetUserNameByEmail(string email);

        int MaxInvalidPasswordAttempts { get; }
        int MinRequiredNonAlphanumericCharacters { get; }
        int MinRequiredPasswordLength { get; }
        int PasswordAttemptWindow { get; }
        MembershipPasswordFormat PasswordFormat { get; }
        string PasswordStrengthRegularExpression { get; }
        bool RequiresQuestionAndAnswer { get; }
        bool RequiresUniqueEmail { get; }
        bool EnablePasswordReset { get; }
        bool EnablePasswordRetrieval { get; }
        string ResetPassword(string username, string passwordAnswer);
        bool ChangePassword(string userName, string newPassword);
        bool ChangePassword(string username, string oldPassword, string newPassword);
        bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
                                             string newPasswordAnswer);
        
        MembershipCreateStatus CreateUser(string userName, string password, string email);
        MembershipUser CreateUser(string username, string password, string email, string passwordQuestion,
                                  string passwordAnswer, bool isActive, object providerUserKey,
                                  out MembershipCreateStatus status);
        User Create(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved);
        
        bool DeleteUser(string username, bool deleteAllRelatedData);                
        bool UnlockUser(string userName);
        void Unlock(int id);
        void UpdateUser(MembershipUser membershipUser);
        bool ValidateUser(string username, string password);
        User SignIn(string userName, string password, bool createPersistentCookie);
        void SignOut();
    }
}
