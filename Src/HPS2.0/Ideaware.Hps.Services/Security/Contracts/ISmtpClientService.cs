﻿using System.Net.Mail;

namespace Ideaware.Hps.Services.Security.Contracts
{
    public interface ISmtpClientService
    {
        void Send(MailMessage mailMessage);
    }
}
