﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Services.Contracts;

namespace Ideaware.Hps.Services.Security.Contracts
{
    public interface IRoleService: IService<Role>
    {        
        void AddUsersToRoles(IList<User> users, IList<Role> roles);
        void AddUserToRole(User user, string roleName);
        void CreateRole(string roleName);
        bool DeleteRole(string roleName, bool throwOnPopulatedRole);       
        IList<Role> GetRolesForUser(string userName);
        IList<User> GetUsersInRole(string roleName);
        IList<User> GetUsersInRole(string roleName, string usernameToMatch);
        bool IsUserInRole(string userName, string roleName);
        void RemoveUsersFromRoles(string[] userNames, string[] roleNames);
        bool RoleExists(string roleName);                        
        Role FindBy(string roleName);                        
        bool Enabled { get;}        
        void Create(string id);
        IEnumerable<string> FindUserNamesByRole(int key);
        IEnumerable<string> FindByUser(User user);
        void RemoveFromAllRoles(User user);
        void RemoveFromRole(User user, string roleName);
    }
}
