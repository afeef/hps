﻿using System.Collections.Specialized;
using System.Web;

namespace Ideaware.Hps.Services.Security.Contracts
{
    public interface ISiteMapProvider
    {
        bool IsInitialized { get; }
        SiteMapNode RootNode { get; }        
        void Initialize(string name, NameValueCollection attributes);
        SiteMapNode BuildSiteMap();                        
    }
}
