﻿namespace Ideaware.Hps.Services.Security.Contracts
{
    public interface IFormsAuthentication
    {
        void SignIn(string userName, bool createPersistentCookie);
        void SignOut();
    }
}
