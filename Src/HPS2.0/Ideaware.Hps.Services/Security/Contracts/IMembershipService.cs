﻿using System.Collections.Specialized;
using System.Web.Security;

namespace Ideaware.Hps.Services.Security.Contracts
{
    public interface IMembershipService
    {        
        string ApplicationName { get; set; }
        int MaxInvalidPasswordAttempts{ get; }             
        int MinRequiredNonAlphanumericCharacters{ get; }                   
        int MinRequiredPasswordLength{ get; }                    
        int PasswordAttemptWindow{ get; }                   
        MembershipPasswordFormat PasswordFormat{ get; }                     
        string PasswordStrengthRegularExpression{ get; }                  
        bool RequiresQuestionAndAnswer{ get; }                    
        bool RequiresUniqueEmail{ get; }                    
        bool EnablePasswordReset{ get; }
        bool EnablePasswordRetrieval { get; }                            
        void Initialize(string name, NameValueCollection config);
        bool ChangePassword(string userName, string newPassword);
        bool ChangePassword(string username, string oldPassword, string newPassword);
        bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
                                             string newPasswordAnswer);
        MembershipCreateStatus CreateUser(string userName, string password, string email);
        MembershipUser CreateUser(string username, string password, string email, string passwordQuestion,
                                  string passwordAnswer, bool isActive, object providerUserKey,
                                  out MembershipCreateStatus status);
        bool DeleteUser(string username, bool deleteAllRelatedData);
        MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords);
        MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
                                                 out int totalRecords);
        MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords);
        int GetNumberOfUsersOnline();
        string GetPassword(string username, string passwordAnswer);
        MembershipUser GetUser(string username, bool userIsOnline);
        MembershipUser GetUser(object providerUserKey, bool userIsOnline);
        string GetUserNameByEmail(string email);
        string ResetPassword(string username, string passwordAnswer);
        bool UnlockUser(string userName);
        void UpdateUser(MembershipUser membershipUser);
        bool ValidateUser(string username, string password);
    }
}
