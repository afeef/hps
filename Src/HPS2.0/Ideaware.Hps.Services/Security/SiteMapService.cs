﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Web;
using Ideaware.Hps.Services.Security.Contracts;

namespace Ideaware.Hps.Services.Security
{
    public class SiteMapService : StaticSiteMapProvider, ISiteMapProvider
    {

        private bool _isInitialized;
        private string _providerName;
        private string _connectionString;
        private string _storedProcedure;
        private double _cacheTime;

        public virtual bool IsInitialized
        {
            get { return _isInitialized; }
        }

        public override SiteMapNode RootNode
        {
            get { return BuildSiteMap(); }
        }

        protected override void Clear()
        {
            lock (this)
            {
                HttpContext.Current.Cache.Remove("rootNode");
                base.Clear();
            }
        }

        public override void Initialize(string name, NameValueCollection attributes)
        {
            if (IsInitialized)
                return;

            base.Initialize(name, attributes);

            // Retrieve the web.config settings.
            _providerName = attributes["providerName"];
            _connectionString = attributes["connectionString"];
            _storedProcedure = attributes["storedProcedure"];
            _cacheTime = Int32.Parse(attributes["cacheTime"]);

            if (String.IsNullOrEmpty(_providerName))
                throw new ArgumentException("The provider name was not found.");

            if (String.IsNullOrEmpty(_connectionString))
                throw new ArgumentException("The connection string was not found.");

            if (String.IsNullOrEmpty(_storedProcedure))
                throw new ArgumentException("The stored procedure name was not found.");

            _isInitialized = true;
        }

        public override SiteMapNode BuildSiteMap()
        {
            SiteMapNode rootNode;

            lock (this)
            {
                // Don't rebuild the map unless needed.
                // If your site map changes often, consider using caching.
                rootNode = HttpContext.Current.Cache["rootNode"] as SiteMapNode;
                if (rootNode == null)
                {
                    // Start with a clean slate.
                    Clear();

                    // Get all the data (using provider-agnostic code).
                    var provider = DbProviderFactories.GetFactory(_providerName);

                    // Use this factory to create a connection.
                    var connection = provider.CreateConnection();

                    connection.ConnectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

                    // Create the command.
                    var command = provider.CreateCommand();

                    command.CommandText = _storedProcedure;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection = connection;

                    // Create the DataAdapter.
                    var adapter = provider.CreateDataAdapter();
                    adapter.SelectCommand = command;

                    // Get the results in a DataSet.
                    var ds = new DataSet();

                    adapter.Fill(ds, "SiteMap");

                    DataTable dtSiteMap = ds.Tables["SiteMap"];
                    DataRow rowRoot = dtSiteMap.Select("ParentSiteMapId IS NULL")[0];

                    rootNode = new SiteMapNode(this, rowRoot["URL"].ToString(), rowRoot["URL"].ToString(), rowRoot["Title"].ToString(), rowRoot["Description"].ToString());

                    var rootId = rowRoot["SiteMapId"].ToString();

                    AddNode(rootNode);
                    // Fill down the hierarchy.
                    AddChildren(rootNode, rootId, dtSiteMap);

                    // Store the root node in the cache.
                    HttpContext.Current.Cache.Insert("rootNode", rootNode,
                    null, DateTime.Now.AddSeconds(_cacheTime), TimeSpan.Zero);
                }
            }
            return rootNode;
        }

        private SiteMapNode CreateNode(SiteMapNode parent, Dictionary<Guid, SiteMapNode> nodes, Guid id, string title, string desc, string url, string[] roles)
        {

            var node = new SiteMapNode(this, id.ToString(), url, title, desc) { Roles = roles };

            if (nodes.ContainsKey(id))
                throw new ConfigurationErrorsException("id already exists");

            nodes.Add(id, node);

            AddNode(node, parent);

            return node;
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            return BuildSiteMap();
        }

        private void AddChildren(SiteMapNode rootNode, string rootId, DataTable dtSiteMap)
        {
            var childRows = dtSiteMap.Select(string.Format("ParentSiteMapId = '{0}'", rootId));
            foreach (DataRow row in childRows)
            {

                var childNode = new SiteMapNode(this,
                    row["Url"].ToString(),
                    row["Url"].ToString(),
                    row["Title"].ToString(),
                    row["Description"].ToString());

                var rowId = row["SiteMapId"].ToString();

                // Use the SiteMapNode AddNode method to add
                // the SiteMapNode to the ChildNodes collection.
                AddNode(childNode, rootNode);
                // Check for children in this node.
                AddChildren(childNode, rowId, dtSiteMap);
            }
        }
    }
}
