﻿using System.Web.Security;
using Ideaware.Hps.Services.Security.Contracts;

namespace Ideaware.Hps.Services.Security
{
    public class FormsAuthenticationService : IFormsAuthentication
    {
        public void SignIn(string userName, bool createPersistentCookie)
        {
            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
