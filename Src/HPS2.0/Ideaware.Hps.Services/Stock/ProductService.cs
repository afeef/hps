﻿using System.Collections.Generic;
using System.Linq;
using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Services.Stock.Contracts;
using Ideaware.Hps.Web.ViewModels;
using MvcContrib.UI.Grid;
using NHibernate;
using NHibernate.Linq;

namespace Ideaware.Hps.Services.Stock
{
    public class ProductService: BaseService<Product>, IProductService
    {
        public ProductService(ISession session) 
            : base(session)
        {
        }

        public IList<Product> FindByGoodReceived(int key)
        {
            var query = from p in this.Session.QueryOver<Product>()
                        where p.GoodsReceived.Key == key
                        select p;

            return query.List();

            //return this.Session.QueryOver<GoodsReceived>().Where(x => x.Key == key).List();
        }

        public PagedViewModel<Product> FindByGoodReceived(GridSortOptions options, string searchWord, int page, int pageSize, int key)
        {
            var query = from c in Session.Query<Product>()
                        where c.GoodsReceived.Key == key
                        select c;

            var pagedViewModel = new PagedViewModel<Product>
            {
                Query = query,
                GridSortOptions = options,
                Page = page,
                PageSize = pageSize
            };

            return pagedViewModel;
        }
    }
}
