﻿using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Services.Stock.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Stock
{
    public class GoodsTransferService: BaseService<GoodsTransfer>, IGoodsTransferService
    {
        public GoodsTransferService(ISession session)
            : base(session)
        {
        }
    }
}
