﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Services.Contracts;
using Ideaware.Hps.Web.ViewModels;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Services.Stock.Contracts
{
    public interface IProductService: IService<Product>
    {
        IList<Product> FindByGoodReceived(int key);

        PagedViewModel<Product> FindByGoodReceived(GridSortOptions options, string searchWord, int page, int pageSize,
                                                   int key);
    }
}
