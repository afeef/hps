﻿using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Services.Contracts;

namespace Ideaware.Hps.Services.Stock.Contracts
{
    public interface IGoodsReceivedService: IService<GoodsReceived>
    {
    }
}
