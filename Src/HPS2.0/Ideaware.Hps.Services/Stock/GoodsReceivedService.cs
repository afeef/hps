﻿using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Services.Stock.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Stock
{
    public class GoodsReceivedService: BaseService<GoodsReceived>, IGoodsReceivedService
    {
        public GoodsReceivedService(ISession session) 
            : base(session)
        {
        }
    }
}
