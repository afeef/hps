﻿using System;
using System.Collections.Generic;
using Ideaware.Hps.Entities;
using Ideaware.Hps.Web.ViewModels;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Services.Contracts
{
    public interface IService<T> where T : EntityBase 
    {
        //void Commit();
        //void Delete<T>(Expression<Func<T, bool>> expression) where T : class, new();
        //void Delete<T>(T item) where T : class, new();
        //void DeleteAll<T>() where T : class, new();
        //T Single<T>(Expression<Func<T, bool>> expression) where T : class, new();
        //T FindBy<T>(Expression<Func<T, bool>> expression) where T : class, new();
        //IList<T> FindAll<T>();
        //IList<T> FindAll<T>(string sortExpression, SortDirection sortDirection, int startRowIndex, int pageSize);
        PagedViewModel<T> FindAll(GridSortOptions options, string searchWord, int page, int pageSize);
        IList<T> FindAll();
        IList<T> FindAll(int pageIndex, int pageSize);
        T FindBy(int key);
        void Save(T entity);
        void Update(T entity);
        bool Delete(int key);
        //void Add<T>(T item) where T : class, new();
        //void Add<T>(IEnumerable<T> items) where T : class, new();
        //IList<T> FindAll<T>() where T: class, new();
        //IList<T> FindAll<T>(int pageIndex, int pageSize);
        //T FindBy<T>(int key);
        //void Save<T>(T branch) where T: class, new();
        //void Update<T>(T branch);
        //int Count { get; }
        //bool Delete<T>(int id);
    }
}
