﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Setup
{
    public class VendorService: BaseService<Vendor>, IVendorService
    {        
        public VendorService(ISession session)
            :base(session)
        {            
        }        
    }
}
