﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Setup
{
    public class TenantService : BaseService<Tenant>, ITenantService
    {
        public TenantService(ISession session)
            : base(session)
        {
            
        }
    }
}
