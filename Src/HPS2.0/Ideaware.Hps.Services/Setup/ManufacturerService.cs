﻿using Ideaware.Hps.Entities.Setup;
using NHibernate;
using Ideaware.Hps.Services.Setup.Contracts;

namespace Ideaware.Hps.Services.Setup
{
    public class ManufacturerService: BaseService<Manufacturer>, IManufacturerService
    {        
        public ManufacturerService(ISession session)
            :base(session)
        {            
        }        
    }
}
