﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Setup
{
    public class DocumentTypeService: BaseService<DocumentType>, IDocumentTypeService
    {
        
        public DocumentTypeService(ISession session)
            :base(session)
        {            
        }        
    }
}
