﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Contracts;

namespace Ideaware.Hps.Services.Setup.Contracts
{
    public interface ICategoryService : IService<Category>
    {        
    }
}
