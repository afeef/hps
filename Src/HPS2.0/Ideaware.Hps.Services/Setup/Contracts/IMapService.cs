﻿using System.Collections.Generic;

namespace Ideaware.Hps.Services.Setup.Contracts
{
    public interface IMapService
    {
        string FindAddress(string lat, string lng);
        IList<decimal> FindLatLng(string address);
        IList<decimal> FindLatLng(string address, string format);
    }
}
