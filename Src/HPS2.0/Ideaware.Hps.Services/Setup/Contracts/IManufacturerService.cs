﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Contracts;

namespace Ideaware.Hps.Services.Setup.Contracts
{
    public interface IManufacturerService: IService<Manufacturer>
    {        
    }
}
