﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using ISession = NHibernate.ISession;

namespace Ideaware.Hps.Services.Setup
{
    public class BranchService : BaseService<Branch>, IBranchService
    {        
        public BranchService(ISession session)
            :base(session)
        {            
        }
    }
}
