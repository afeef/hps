﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Setup
{
    public class ModelService: BaseService<Model>, IModelService
    {        
        public ModelService(ISession session)
            :base(session)
        {            
        }        
    }
}
