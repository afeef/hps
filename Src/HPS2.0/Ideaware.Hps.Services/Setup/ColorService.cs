﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Setup
{
    public class ColorService :BaseService<Color>, IColorService
    {                
        public ColorService(ISession session)
            : base(session)
        {            
        }                       
    }
}
