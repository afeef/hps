﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Services.Setup.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Setup
{
    public class CategoryService:BaseService<Category>, ICategoryService
    {
        
        public CategoryService(ISession session)
            :base(session)
        {            
        }                                     
    }
}
