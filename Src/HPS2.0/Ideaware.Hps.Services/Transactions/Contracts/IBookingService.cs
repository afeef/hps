﻿using Ideaware.Hps.Entities.Transactions;
using Ideaware.Hps.Services.Contracts;

namespace Ideaware.Hps.Services.Transactions.Contracts
{
    public interface IBookingService: IService<Booking>
    {
    }
}
