﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ideaware.Hps.Entities.Transactions;
using Ideaware.Hps.Services.Contracts;
using Ideaware.Hps.Web.ViewModels;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Services.Transactions.Contracts
{
    public interface IInstallmentPlanService: IService<InstallmentPlan>
    {
        IList<InstallmentPlan> FindByBooking(int key);

        PagedViewModel<InstallmentPlan> FindByBooking(GridSortOptions options, string searchWord, int page, int pageSize,
                                                   int key);
    }
}
