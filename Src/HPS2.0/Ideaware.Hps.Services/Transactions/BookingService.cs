﻿using Ideaware.Hps.Entities.Transactions;
using Ideaware.Hps.Services.Transactions.Contracts;
using NHibernate;

namespace Ideaware.Hps.Services.Transactions
{
    public class BookingService : BaseService<Booking>, IBookingService
    {
        public BookingService(ISession session) : base(session)
        {
        }
    }
}
