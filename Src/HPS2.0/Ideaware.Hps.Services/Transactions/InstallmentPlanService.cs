﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ideaware.Hps.Entities.Transactions;
using Ideaware.Hps.Services.Transactions.Contracts;
using Ideaware.Hps.Web.ViewModels;
using MvcContrib.UI.Grid;
using NHibernate;
using NHibernate.Linq;

namespace Ideaware.Hps.Services.Transactions
{
    public class InstallmentPlanService: BaseService<InstallmentPlan>, IInstallmentPlanService
    {
        public InstallmentPlanService(ISession session) : base(session)
        {
        }

        public IList<InstallmentPlan> FindByBooking(int key)
        {
            var query = from p in this.Session.QueryOver<InstallmentPlan>()
                        where p.Booking.Key == key
                        select p;

            return query.List();

            //return this.Session.QueryOver<GoodsReceived>().Where(x => x.Key == key).List();
        }

        public PagedViewModel<InstallmentPlan> FindByBooking(GridSortOptions options, string searchWord, int page, int pageSize, int key)
        {
            var query = from c in Session.Query<InstallmentPlan>()
                        where c.Booking.Key == key
                        select c;

            var pagedViewModel = new PagedViewModel<InstallmentPlan>
            {
                Query = query,
                GridSortOptions = options,
                Page = page,
                PageSize = pageSize
            };

            return pagedViewModel;
        }
    }
}
