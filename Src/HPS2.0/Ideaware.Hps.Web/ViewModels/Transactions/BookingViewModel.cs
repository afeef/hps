﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Entities.Transactions;

namespace Ideaware.Hps.Web.ViewModels.Transactions
{
    public class BookingViewModel : BaseViewModel
    {
        public int BranchId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public string BranchName { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public Booking Booking { get; set; }
        public Product Product { get; set; }        
        public IList<User> Guarantors { get; set; }
        public string GuarantorKeys { get; set; } 
        public IList<InstallmentPlan> Installments { get; set; } 

        public BookingViewModel()
        {
        }

        public BookingViewModel(Booking booking, Product product,
            IList<User> guarantors,
            IList<InstallmentPlan> installments )
        {
            this.Booking = booking;
            this.Product = product;
            this.Guarantors = guarantors;
            this.Installments = installments;
        }
    }
}
