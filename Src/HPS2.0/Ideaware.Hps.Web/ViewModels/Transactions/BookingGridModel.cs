﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ideaware.Hps.Entities.Transactions;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Transactions
{
    public class BookingGridModel : GridModel<Booking>
    {
        public BookingGridModel()
        {
            Column.For(x => x.AccountNo);
            Column.For(x => x.Customer.FullName);
            Column.For(x => x.Product.ProductName);
            Column.For(x => x.BookingDate);
            Column.For(x => x.SalePrice);
            Column.For(x => x.NoOfInstallments);            
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");            
            this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessBookingId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<Booking>());
        }
    }
}
