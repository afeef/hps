﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Web.ViewModels.Setup
{
    public class ModelViewModel
    {
        public Model Model { get; set; }
        public int CategoryId { get; set; }
        public int ManufacturerId { get; set; }
        public SelectList Categories { get; set; }
        public SelectList Manufacturers { get; set; }

        public ModelViewModel()
        {

        }

        public ModelViewModel(Model model, IEnumerable<Category> categories, IEnumerable<Manufacturer> manufacturers  )
        {
            this.Model = model;
            this.Categories = new SelectList(categories, "Key", "CategoryName", this.CategoryId);
            this.Manufacturers = new SelectList(manufacturers, "Key", "ManufacturerName", this.ManufacturerId);
        }
    }
}
