﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Setup
{
    public class DocumentTypeGridModel : GridModel<DocumentType>
    {
        public DocumentTypeGridModel()
        {
            this.Column.For(x => x.DocumentName);
            this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);
            this.Column.For(x => x.AccessUserId);

            this.RenderUsing(new HtmlTableGridThemedRenderer<DocumentType>());
        }
    }
}
