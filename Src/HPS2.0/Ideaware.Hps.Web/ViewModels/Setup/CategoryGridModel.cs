﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Setup
{
    public class CategoryGridModel : GridModel<Category>
    {
        public CategoryGridModel()
        {
            Column.For(x => x.CategoryName);
            Column.For(x => x.Color);
            this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            Column.For(x => x.IsActive);            

            RenderUsing(new HtmlTableGridThemedRenderer<Category>());
        }        
    }
}
