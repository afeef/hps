﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Setup
{
    public class VendorGridModel : GridModel<Vendor>
    {
        public VendorGridModel()
        {
            this.Column.For(x => x.VendorName);
            this.Column.For(x => x.ContactPerson);
            this.Column.For(x => x.ContactNumber);
            this.Column.For(x => x.FaxNumber);
            this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            this.Column.For(x => x.IsActive);
            //this.Column.For(x => x.AccessUserId);

            this.RenderUsing(new HtmlTableGridThemedRenderer<Vendor>());
        }
    }
}
