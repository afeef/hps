﻿using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Setup
{
    public class BranchGridModel : GridModel<Branch>
    {
        public BranchGridModel()
        {
            Column.For(x => x.BranchName);
            Column.For(x => x.ContactNumber);
            Column.For(x => x.OfficeAddress);
            Column.For(x => x.FaxNumber);
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");
            Column.For(x => x.IsBlock);
            this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<Branch>());
        }

        private static string GetCityLink(string city)
        {
            return string.IsNullOrWhiteSpace(city)
                       ? string.Empty
                       : string.Format("<a href=\"http://www.bing.com/search?q={0}\" target=\"_blank\">{0}</a>", city);
        }
    }
}
