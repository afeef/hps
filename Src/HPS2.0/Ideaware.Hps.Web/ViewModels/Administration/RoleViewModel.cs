﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Web.ViewModels.Administration
{
	public class RoleViewModel
	{
		public string Role { get; set; }
		public IDictionary<string, User> Users { get; set; }
	}
}