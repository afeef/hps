﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Web.ViewModels.Administration
{
    public class UserViewModel
    {                
        public User User { get; set; }
        public int BranchId { get; set; }
        public SelectList Branches { get; set; }

        [Display(Name = "Initial Roles")]
        public IDictionary<string, bool> InitialRoles { get; set; }

        public UserViewModel()
        {

        }

        public UserViewModel(User user, IEnumerable<Branch> branches, Dictionary<string, bool> initialRoles)
        {
            this.User = user;
            this.Branches = new SelectList(branches, "Key", "BranchName", this.BranchId);
            this.InitialRoles = initialRoles;
        }        
    }
}
