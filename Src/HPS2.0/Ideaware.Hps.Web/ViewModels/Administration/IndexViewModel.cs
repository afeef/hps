﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Web.ViewModels.Administration
{
	public class IndexViewModel
	{
		public string Search { get; set; }
		public IList<User> Users { get; set; }
		public IList<Role> Roles { get; set; }
		public bool IsRolesEnabled { get; set; }
	}
}