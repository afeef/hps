﻿using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Administration
{    
    public class UserGridModel : GridModel<User>
    {
        public UserGridModel()
        {
            Column.For(x => x.UserName);
            Column.For(x => x.Email);
            Column.For(x => x.OfficeAddress);
            Column.For(x => x.MultiGuarantee);
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");            
            this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<User>());
        }       
    }
}
