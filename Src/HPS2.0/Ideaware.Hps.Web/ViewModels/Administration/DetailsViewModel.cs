﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Web.ViewModels.Administration
{
	public class DetailsViewModel
	{
		#region StatusEnum enum

		public enum StatusEnum
		{
			Offline,
			Online,
			LockedOut,
			Unapproved
		}

		#endregion

		public string DisplayName { get; set; }
		public StatusEnum Status { get; set; }
		public User User { get; set; }
		public bool CanResetPassword { get; set; }
		public bool RequirePasswordQuestionAnswerToResetPassword { get; set; }
		public IDictionary<Role, bool> Roles { get; set; }
		public bool IsRolesEnabled { get; set; }
	}
}