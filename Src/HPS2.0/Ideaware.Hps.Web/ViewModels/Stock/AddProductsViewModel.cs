﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Entities.Stock;

namespace Ideaware.Hps.Web.ViewModels.Stock
{
    public class AddProductsViewModel
    {
        public PagedViewModel<Product> Products { get; set; }
        public Product Product { get; set; }
        public SelectList Manufacturers { get; set; }
        public SelectList Models { get; set; }
        public SelectList Branches { get; set; }
        public SelectList Colors { get; set; }
        public int ManufacturerId { get; set; }
        public int ModelId { get; set; }
        public int BranchId { get; set; }
        public int ColorId { get; set; }
        public int GoodsReceivedId { get; set; }

        public AddProductsViewModel()
        {

        }

        public AddProductsViewModel(int goodsReceivedId,
                PagedViewModel<Product> products,
                IEnumerable<Manufacturer> manufacturers,
                IEnumerable<Model> models,
                IEnumerable<Branch> branches,
                IEnumerable<Color> colors)
        {
            this.Product = new Product();
            this.GoodsReceivedId = goodsReceivedId;
            this.Products = products;
            this.Manufacturers = new SelectList(manufacturers, "Key", "ManufacturerName", this.ManufacturerId);
            this.Models = new SelectList(models, "Key", "ModelName", this.ModelId);
            this.Branches = new SelectList(branches, "Key", "BranchName", this.BranchId);
            this.Colors = new SelectList(colors, "Key", "ColorName", this.ColorId);
        }
    }
}
