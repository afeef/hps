﻿using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Stock
{
    public class GoodsReceivedGridModel : GridModel<GoodsReceived>
    {
        public GoodsReceivedGridModel()
        {
            this.Column.For(x => x.GoodsReceivedNo);
            this.Column.For(x => x.GoodsReceivedDate.ToShortDateString()).Named("Received on").SortColumnName("GoodsReceivedDate");
            this.Column.For(x => x.Category.CategoryName);
            this.Column.For(x => x.Vendor.VendorName);            
            this.Column.For(x => x.Remarks);            
            this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");
            //this.Column.For(x => x.IsActive);            
            //this.Column.For(x => x.AccessUserId);            

            this.RenderUsing(new HtmlTableGridThemedRenderer<GoodsReceived>());
        }
    }
}
