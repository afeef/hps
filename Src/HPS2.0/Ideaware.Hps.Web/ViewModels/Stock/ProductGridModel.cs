﻿using Ideaware.Hps.Entities.Stock;
using Ideaware.Hps.Web.UI.Themed.Grid;
using MvcContrib.UI.Grid;

namespace Ideaware.Hps.Web.ViewModels.Stock
{
    public class ProductGridModel : GridModel<Product>
    {
        public ProductGridModel()
        {
            Column.For(x => x.ProductName);
            Column.For(x => x.Branch.BranchName);
            Column.For(x => x.Manufacturer.ManufacturerName);
            Column.For(x => x.Model.ModelName);
            Column.For(x => x.Color.ColorName);
            //Column.Custom(x => GetCityLink(x.BlockDetail)).Named("BlockDetail").SortColumnName("BlockDetail");
            Column.For(x => x.SalePrice);
            Column.For(x => x.ActualPrice);
            //this.Column.For(x => x.DateCreated.ToShortDateString()).Named("Created").SortColumnName("DateCreated");
            //this.Column.For(x => x.DateModified.ToShortDateString()).Named("Modified").SortColumnName("DateModified");            
            //Column.For(x => x.IsActive);
            //Column.For(x => x.Fax);
            //Column.For(x => x.AccessUserId);
            //Column.For(x => x.SupportRepresentative).Named("Support Rep");

            RenderUsing(new HtmlTableGridThemedRenderer<Product>());
        }        
    }
}
