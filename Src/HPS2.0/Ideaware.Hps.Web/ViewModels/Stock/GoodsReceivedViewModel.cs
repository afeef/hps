﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Entities.Stock;

namespace Ideaware.Hps.Web.ViewModels.Stock
{
    public class GoodsReceivedViewModel
    {
        public GoodsReceived GoodsReceived { get; set; }
        public SelectList Categories { get; set; }
        public SelectList Vendors { get; set; }        
        public int CategoryId { get; set; }
        public int VendorId { get; set; }

        public GoodsReceivedViewModel()
        {

        }

        public GoodsReceivedViewModel(GoodsReceived goodsReceived,
            IEnumerable<Category> categories,
            IEnumerable<Vendor> vendors)
        {
            this.GoodsReceived = goodsReceived;
            this.Categories = new SelectList(categories, "Key", "CategoryName", this.CategoryId);
            this.Vendors = new SelectList(vendors, "Key", "VendorName", this.VendorId);          
        }
    }
}
