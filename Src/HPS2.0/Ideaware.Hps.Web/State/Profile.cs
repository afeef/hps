﻿using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Web.State
{
    public class Profile
    {
        public Profile(User user)
        {
            this.Id = user.Key;
            this.UserName = user.UserName;

            if (user.Roles.Count > 0)
                this.Role = user.Roles[0].RoleName;

            this.Email = user.Email;
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }

        public User ToUser()
        {
            return new User
            {
                Key = this.Id
            };
        }
    }
}
