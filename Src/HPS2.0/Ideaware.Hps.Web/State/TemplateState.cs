﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Ideaware.Hps.Common.Configuration;

namespace Ideaware.Hps.Web.State
{
	public class TemplateState
	{
		public TemplateState(string name)
		{
			this.Name = name;
			this.Items = new List<TemplateItem>();
		}

		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the relative path based on the name
		/// </summary>
		public string Path
		{
			get
			{
				return SiteConfiguration.Current.Template.Path + Name;
			}
		}

		/// <summary>
		/// Gets the relative path based on the name
		/// </summary>
		public string ImagePath
		{
			get
			{
				return this.Path + "/images";
			}
		}

		public List<TemplateItem> Items
		{
			get;
			set;
		}

		#region TemplateItem and TemplateItem type declarations
		public class TemplateItem
		{
			public TemplateItem(string fullText)
			{
				this.Type = TemplateItemType.Text;
				
				//parse the fullText
				if (fullText.StartsWith("{-"))
				{
					this.Value = Regex.Replace(fullText, @"{-(\w+?)-}", "$1");
					if (this.Value.Contains("Content"))
					{
						this.Type = TemplateItemType.Container;
					}
					else
					{
						this.Type = TemplateItemType.Partial;
					}
				}
				else
				{
					this.Value = fullText;
				}
			}

			public string Value
			{
				get;
				set;
			}

			public TemplateItemType Type
			{
				get;
				set;
			}
		}

		public enum TemplateItemType
		{
			Text,
			Partial,
			Container
		}
		#endregion
	}
}
