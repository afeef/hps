﻿using System.Web.Mvc.Html;
using Ideaware.Hps.Web.State;

namespace Ideaware.Hps.Web.UI
{
	public class TemplatedLayout : BaseViewPage
	{
		public override void ExecutePageHierarchy()
		{
			base.ExecutePageHierarchy();
			foreach (TemplateState.TemplateItem item in this.Cache.Template.Items)
			{
				WriteItem(item);
			}
		}

		protected virtual void WriteItem(TemplateState.TemplateItem item)
		{
			if (item.Type == TemplateState.TemplateItemType.Text)
			{
				WriteLiteral(item.Value);
			}
			else if (item.Type == TemplateState.TemplateItemType.Container)
			{
				switch (item.Value.ToUpper()) //Name
				{
					case "MAINCONTENT":
						Write(RenderBody());
						break;
					case "HEADCONTENT":
						if (IsSectionDefined("Head"))
						{
							Write(RenderSection("Head"));
						}
						break;
				}
			}
			else if (item.Type == TemplateState.TemplateItemType.Partial)
			{
				Write(this.Html.Partial(item.Value, this.ViewData));
			}
		}
	}
}
