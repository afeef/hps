﻿using System;
using System.Linq;
using Ideaware.Hps.Entities.Enum;
using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Web.Extensions
{
    public static class UserExtensions
    {
        public static bool HasPermissions(this User user, Permissions required)
        {

            return user.Roles.SelectMany(role => role.Permissions)
                .Any(permission => permission.PermissionFlag.Equals(required));
        }
    }
}
