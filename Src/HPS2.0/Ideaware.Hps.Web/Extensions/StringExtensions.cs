﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Ideaware.Hps.Common;
using Ideaware.Hps.Common.Configuration;

namespace Ideaware.Hps.Web.Extensions
{
	public static class StringExtensions
	{
		/// <summary>
		/// Replace pattern (determined by the configuration files) of in the text
		/// </summary>
		public static string ReplaceValues(this string value)
		{
			if (SiteConfiguration.Current != null)
			{
				ReplacementCollection replacements = SiteConfiguration.Current.Replacements;
				foreach (ReplacementItem item in replacements)
				{
					value = Regex.Replace(value, item.Pattern, item.Replacement, item.RegexOptions);
				}
			}
			return value;
		}

		/// <summary>
		/// Parses HTML to avoid XSS attacks, based on the site configuration
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string SafeHtml(this string value)
		{
			var htmlConfig = SiteConfiguration.Current.SpamPrevention.HtmlInput;
			if (String.IsNullOrEmpty(value))
			{
				return value;
			}
			var html = DataHelper.SanitizeHtml(value, htmlConfig.AllowedElements);
			if (htmlConfig.FixErrors && html.Length > 0)
			{
				const string wrapperStart = "<div class=\"htmlWrapper\">";
				const string wrapperEnd = "</div>";
				var outputWriter = new StringWriter();
				var doc = new HtmlDocument();
				doc.LoadHtml(wrapperStart + html + wrapperEnd);
				doc.OptionWriteEmptyNodes = true;
				doc.Save(outputWriter);
				html = outputWriter.ToString();
				html = html.Remove(html.Length - wrapperEnd.Length);
				html = html.Remove(0, wrapperStart.Length);
			}
            html = DataHelper.HtmlLinkAddNoFollow(html);
			return html;
		}

		public static string FirstUpperCase(this string value)
		{
			if (!String.IsNullOrEmpty(value))
			{
				string result = value.Substring(0, 1);
				result = result.ToUpper();

				if (value.Length > 1)
				{
					result += value.Substring(1);
				}
				return result;
			}
			return value;
		}
	}
}
