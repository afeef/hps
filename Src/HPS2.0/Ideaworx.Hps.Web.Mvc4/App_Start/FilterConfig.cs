﻿using System.Web;
using System.Web.Mvc;

namespace Ideaworx.Hps.Web.Mvc4
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}