﻿using System.Collections.Generic;
using Ideaware.Hps.Common.Validation;
using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Entities
{
    public class Forum : EntityBase
	{
		public int Id
		{
			get;
			set;
		}

		[RequireField]
		[Length(255)]
		public string Name
		{
			get;
			set;
		}

		[RequireField]
		[Length(32)]
		public string ShortName
		{
			get;
			set;
		}

		
		/// <summary>
		/// Plain text forum description
		/// </summary>
		[RequireField]
        public string Description
		{
			get;
			set;
		}

		[RequireField]
		public ForumCategory Category
		{
			get;
			set;
		}

		public List<Topic> Topics
		{
			get;
			set;
		}

		public int TopicCount
		{
			get;	
			set;
		}

		public int MessageCount
		{
			get;
			set;
		}
	}
}
