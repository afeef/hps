﻿using System;

namespace Ideaware.Hps.Entities.Reports
{
    public class InstallmentReport : EntityBase, IComparable<InstallmentReport>
    {        
        public long InstallmentCreditId { get; set; }
        public string PersonName { get; set; }
        public string BranchName { get; set; }
        public DateTime InstallmentDate { get; set; }
        public int InstallmentAmount { get; set; }
        public string ReceiptNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public int AmountPaid { get; set; }
        public int FinePaid { get; set; }
        public string AccountNo { get; set; }        

        #region IComparable<InstallmentReports> Members

        public int CompareTo(InstallmentReport report)
        {
            return this.BranchName.CompareTo(report.BranchName);
        }

        #endregion
    }
}
