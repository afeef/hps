﻿using System.ComponentModel.DataAnnotations;

namespace Ideaware.Hps.Entities.Setup {

    public class Color : EntityBase{
        [Required]
        [Display(Name = "Color name")]
        public virtual string ColorName { get; set; }        
    }
}
