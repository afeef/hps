﻿using System.ComponentModel.DataAnnotations;

namespace Ideaware.Hps.Entities.Setup
{
    public class Tenant: EntityBase
    {
        [Required]
        public virtual string TenantName { get; set; }
        public virtual string TenantDescription { get; set; }
    }
}
