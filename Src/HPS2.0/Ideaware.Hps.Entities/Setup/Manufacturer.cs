﻿namespace Ideaware.Hps.Entities.Setup
{
    public class Manufacturer : EntityBase
    {
        public virtual string ManufacturerName { get; set; }
        public virtual string ContactPerson { get; set; }
        public virtual string OfficeAddress { get; set; }
        public virtual string ContactNumber { get; set; }
        public virtual string FaxNumber { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual bool IsBlock { get; set; }
        public virtual string BlockDetail { get; set; }
    }
}
