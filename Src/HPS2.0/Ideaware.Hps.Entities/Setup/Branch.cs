﻿namespace Ideaware.Hps.Entities.Setup
{
    public class Branch : EntityBase
    {
        public virtual string BranchName { get; set; }
        public virtual string OfficeAddress { get; set; }
        public virtual int ContactNumber { get; set; }
        public virtual int FaxNumber { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual bool IsBlock { get; set; }
        public virtual string BlockDetail { get; set; }
    }
}
