﻿using System.ComponentModel.DataAnnotations;

namespace Ideaware.Hps.Entities.Setup
{
    public class Category : EntityBase
    {
        [Required]
        [Display(Name = "Category name")]
        public virtual string CategoryName { get; set; }        
        public virtual bool EngineNo { get; set; }
        public virtual bool ChasisNo { get; set; }
        public virtual bool RegistrationNo { get; set; }
        public virtual bool ProductSize { get; set; }
        public virtual bool ProductWeight { get; set; }
        public virtual bool EmiNo { get; set; }
        public virtual bool ManufacturingDate { get; set; }
        public virtual bool ValidityDate { get; set; }
        public virtual bool KeyNo { get; set; }        
        public virtual bool Color { get; set; }
    }
}
