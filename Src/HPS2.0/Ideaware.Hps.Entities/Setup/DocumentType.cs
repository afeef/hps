﻿using System.ComponentModel.DataAnnotations;

namespace Ideaware.Hps.Entities.Setup {

    public class DocumentType : EntityBase{
        [Required]
        [Display(Name = "Document name")]
        public virtual string DocumentName { get; set; }       
    }
}
