﻿using System.ComponentModel.DataAnnotations;

namespace Ideaware.Hps.Entities.Setup
{
    public class Model : EntityBase
    {                
        [Required]
        [Display(Name = "Model name")]
        public virtual string ModelName { get; set; }
        public virtual Category Category { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }        
    }
}
