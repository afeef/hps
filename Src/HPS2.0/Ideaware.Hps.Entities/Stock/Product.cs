﻿using System;
using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Entities.Stock {

    public class Product : EntityBase{               

        public virtual GoodsReceived GoodsReceived { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual Model Model { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual Color Color { get; set; }
        public virtual string ProductNo { get; set; }
        public virtual string ProductName { get; set; }
        public virtual string EngineNo { get; set; }
        public virtual string ChasisNo { get; set; }
        public virtual string RegistrationNo { get; set; }
        public virtual string ProductSize { get; set; }
        public virtual string ProductWeight { get; set; }
        public virtual string EMINo { get; set; }
        public virtual DateTime ManufacturingDate { get; set; }
        public virtual DateTime ValidityDate { get; set; }
        public virtual string KeyNo { get; set; }
        public virtual int ActualPrice { get; set; }
        public virtual int SalePrice { get; set; }
        public virtual int ProductStatus { get; set; }
        public virtual string ProductRemarks { get; set; }
    }
}
