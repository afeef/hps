﻿using System;
using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Entities.Stock {

    public class GoodsReceived : EntityBase {
               
        public virtual string GoodsReceivedNo { get; set; }
        public virtual DateTime GoodsReceivedDate { get; set; }
        public virtual Category Category { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual string Remarks { get; set; }
    }
}
