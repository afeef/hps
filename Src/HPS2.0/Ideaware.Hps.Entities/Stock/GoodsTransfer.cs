﻿using System;

namespace Ideaware.Hps.Entities.Stock {

    public class GoodsTransfer : EntityBase {       

        public string GoodsTransferNo { get; set; }
        public DateTime GoodsTransferDate { get; set; }
        public int GoodsTransferFrom { get; set; }
        public int GoodsTransferTo { get; set; }
        public string GoodsTransferRemarks { get; set; }
    }
}
