﻿using System;
using System.Collections.Generic;
using Ideaware.Hps.Entities.Security;
using Ideaware.Hps.Entities.Setup;
using Ideaware.Hps.Entities.Stock;

namespace Ideaware.Hps.Entities.Transactions
{
    public class Booking : EntityBase
    {
        public virtual Branch Branch { get; set; }
        public virtual string AccountNo { get; set; }
        public virtual User Customer { get; set; }
        public virtual Product Product { get; set; }
        public virtual DateTime BookingDate { get; set; }
        public virtual int SalePrice { get; set; }
        public virtual int ServiceCharges { get; set; }
        public virtual int DiscountAmount { get; set; }
        public virtual int AdvanceAmount { get; set; }
        public virtual int NoOfInstallments { get; set; }
        public virtual int ModeOfPayment { get; set; }
        public virtual string ChequeNo { get; set; }
        public virtual IList<User> Guarantors { get; set; }
    }
}
