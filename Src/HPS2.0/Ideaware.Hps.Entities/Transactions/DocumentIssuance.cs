﻿using System;
using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Entities.Transactions {

    public class DocumentIssuance : EntityBase {
              
        public Branch Branch { get; set; }
        public DateTime IssuanceDate { get; set; }
        public DocumentType DocumentType { get; set; }
        public string DocumentNo { get; set; }
        public int CopyStatus { get; set; }
        public int NumberOfCopies { get; set; }
        public string ReceivingPerson { get; set; }
        public string IssuedBy { get; set; }             
    }
}
