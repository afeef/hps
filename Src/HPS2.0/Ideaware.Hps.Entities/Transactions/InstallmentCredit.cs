﻿using System;
using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Entities.Transactions {

    public class InstallmentCredit : EntityBase{

        public Branch Branch { get; set; }
        public Booking Booking { get; set; }
        public InstallmentPlan InstallmentPlan { get; set; }
        public string ReceiptNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public int AmountPaid { get; set; }
        public int FinePaid { get; set; }
        public string ModeOfPayment { get; set; }
        public string ChequeDetail { get; set; }
        public string InstallmentRemarks { get; set; }
    }
}