﻿namespace Ideaware.Hps.Entities.Transactions
{

    public class Balance : EntityBase
    {        
        public Booking Booking { get; set; }
        public int BalanceAmount { get; set; }        
    }
}
