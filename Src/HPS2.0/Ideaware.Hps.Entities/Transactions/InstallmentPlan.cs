﻿using System;

namespace Ideaware.Hps.Entities.Transactions
{
    public class InstallmentPlan : EntityBase
    {
        public virtual Booking Booking { get; set; }
        public virtual InstallmentPlan ParentPlan { get; set; }
        public virtual int InstallmentNo { get; set; }
        public virtual DateTime InstallmentDate { get; set; }
        public virtual int InstallmentAmount { get; set; }
        public virtual int ProfitAmount { get; set; }
        public virtual bool IsComplete { get; set; }
    }
}