﻿namespace Ideaware.Hps.Entities.Models
{
    public class TabViewModel
    {
        public string TabText1 { get; set; }
        public string TabText2 { get; set; }
        public string TabText3 { get; set; }
    }
}
