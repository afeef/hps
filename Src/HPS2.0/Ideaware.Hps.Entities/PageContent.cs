﻿using Ideaware.Hps.Common.Validation;
using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Entities
{
	/// <summary>
	/// Represents a timeless content of the site like about pages / privacy policies /etc.
	/// </summary>
    public class PageContent : EntityBase
	{
		/// <summary>
		/// Creates a new instance of a timeless content.
		/// </summary>
		public PageContent()
		{

		}

		[RequireField]
		public string Body
		{
			get;
			set;
		}

		[RequireField]
		public string Title
		{
			get;
			set;
		}

		/// <summary>
		/// Short name identifier of the page. 
		/// </summary>
		[RequireField]
		[Length(128)]
		public string ShortName
		{
			get;
			set;
		}
	}
}
