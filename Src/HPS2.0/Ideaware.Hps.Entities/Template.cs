﻿using Ideaware.Hps.Common.Validation;
using Ideaware.Hps.Entities.Security;

namespace Ideaware.Hps.Entities
{
	public class Template : EntityBase
	{
		public int Id
		{
			get;
			set;
		}

		private string _key;
		[RequireField]
		[RegexFormat("[\\w-]{1,16}")]
		public string Key
		{
			get
			{
				return _key;
			}
			set
			{
				if (value != null)
				{
					_key = value.ToLower();
				}
				else
				{
					_key = null;
				}
			}
		}

		public string Description
		{
			get;
			set;
		}

		public bool IsCurrent
		{
			get;
			set;
		}
	}
}
