﻿using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Entities.Utility
{
    public class DisplayModel : EntityBase
    {       
        public Model Model { get; set; }
        public int ModelYear { get; set; }
        public int ModelPicture { get; set; }             
    }
}
