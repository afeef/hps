﻿using System;
using Ideaware.Hps.Common.Validation;

namespace Ideaware.Hps.Entities
{
    public interface IEntity: IEnsureValidation
    {
        int Key { get; }
        bool IsActive { get; set; }
        DateTime DateCreated { get; set; }
        DateTime DateModified { get; set; }        
        int AccessUserId { get; set; }
    }
}
