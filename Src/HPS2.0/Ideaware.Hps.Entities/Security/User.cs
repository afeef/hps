﻿using System;
using System.Collections.Generic;
using Ideaware.Hps.Entities.Setup;

namespace Ideaware.Hps.Entities.Security
{
    public class User : EntityBase
    {
        public virtual Tenant Tenant { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual string FullName { get { return string.Format("{0} {1}", this.FirstName, this.LastName); } }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual string Email { get; set; }
        public virtual string PasswordQuestion { get; set; }
        public virtual string PasswordAnswer { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FatherName { get; set; }
        public virtual long Cnic { get; set; }
        public virtual string Occupation { get; set; }
        public virtual string Designation { get; set; }
        public virtual string ResidentialAddress { get; set; }
        public virtual string City { get; set; }
        public virtual string OfficeAddress { get; set; }
        public virtual int MonthlyIncome { get; set; }
        public virtual int OfficePhone { get; set; }
        public virtual int ResidentialPhone { get; set; }
        public virtual int CellNo { get; set; }
        public virtual int FaxNo { get; set; }
        public virtual bool MultiGuarantee { get; set; }
        public virtual string Photo { get; set; }
        public virtual bool IsOnline { get; set; }
        public virtual bool IsApproved { get; set; }
        public virtual bool IsLockedOut { get; set; }
        public virtual DateTime LastPasswordChangedDate { get; set; }
        public virtual DateTime LastLockoutDate { get; set; }
        public virtual DateTime LastActivityDate { get; set; }
        public virtual DateTime LastLoginDate { get; set; }
        public virtual IList<Role> Roles { get; set; }
    }
}
