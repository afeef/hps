﻿using System.Collections.Generic;
using Ideaware.Hps.Entities.Enum;

namespace Ideaware.Hps.Entities.Security
{
    public class Permission : EntityBase
    {
        public virtual string PermissionName { get; set; }
        public virtual Permissions PermissionFlag { get; set; }
        public virtual string PermissionDescription { get; set; }        
        public virtual IList<Role> Roles { get; set; } 
    }
}
