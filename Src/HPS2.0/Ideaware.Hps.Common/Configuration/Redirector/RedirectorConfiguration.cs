﻿using System.Configuration;

namespace Ideaware.Hps.Common.Configuration.Redirector
{
	public class RedirectorConfiguration : ConfigurationSection
	{
		[ConfigurationProperty("urlGroups", IsRequired = true)]
		public RedirectorUrlGroupCollection UrlGroups
		{
			get
			{
				return (RedirectorUrlGroupCollection)this["urlGroups"];
			}
			set
			{
				this["urlGroups"] = value;
			}
		}

		[ConfigurationProperty("ignoreRegex", IsRequired=false)]
		public string IgnoreRegex
		{
			get
			{
				return (string)this["ignoreRegex"];
			}
			set
			{
				this["ignoreRegex"] = value;
			}
		} 

		public RedirectorConfiguration()
		{

		}

		public static RedirectorConfiguration Current
		{
			get
			{
				return (RedirectorConfiguration)ConfigurationManager.GetSection("redirector");
			}
		}
	}


}
