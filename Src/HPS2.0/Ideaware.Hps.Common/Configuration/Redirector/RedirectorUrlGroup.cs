﻿using System.Configuration;

namespace Ideaware.Hps.Common.Configuration.Redirector
{
	public class RedirectorUrlGroup : ConfigurationElement
	{
		[ConfigurationProperty("regex", IsRequired = true)]
		public string Regex
		{
			get
			{
				return (string)this["regex"];
			}
			set
			{
				this["regex"] = value;
			}
		}

		[ConfigurationProperty("urls", IsRequired=true)]
		public RedirectorUrlCollection Urls
		{
			get
			{
				return (RedirectorUrlCollection)this["urls"];
			}
			set
			{
				this["urls"] = value;
			}
		}

		public RedirectorUrlGroup()
		{

		}
	}
}
