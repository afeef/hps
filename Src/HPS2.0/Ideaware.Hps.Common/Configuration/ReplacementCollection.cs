﻿using System.Configuration;

namespace Ideaware.Hps.Common.Configuration
{
	public class ReplacementCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new ReplacementItem();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((ReplacementItem)(element)).Pattern;
		}

		public ReplacementItem this[int index]
		{
			get
			{
				return (ReplacementItem)BaseGet(index);
			}
		}
	}
}
