﻿using System;
using System.Configuration;

namespace Ideaware.Hps.Common.Configuration
{
	/// <summary>
	/// Represents a configuration element for Open Id relying party
	/// </summary>
	public class SSOOpenIdElement : ConfigurationElement, IOptionalElement
	{
		/// <summary>
		/// Use this property for fixed identifiers for custom sso like login.yoursite.com
		/// </summary>
		[ConfigurationProperty("identifier", IsRequired = false)]
		public string Identifier
		{
			get
			{
				return (string)this["identifier"];
			}
			set
			{
				this["identifier"] = value;
			}
		}

		/// <summary>
		/// Determines if the provider required data has been defined.
		/// </summary>
		public bool IsDefined
		{
			get
			{
				return !String.IsNullOrEmpty(this.Identifier);
			}
		}
	}
}
