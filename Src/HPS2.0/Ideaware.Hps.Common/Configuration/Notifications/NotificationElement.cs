﻿using System;
using System.Configuration;

namespace Ideaware.Hps.Common.Configuration.Notifications
{
	public class NotificationElement : ConfigurationElement, IOptionalElement
	{
		[ConfigurationProperty("body", IsRequired = false)]
		public CDataElement Body
		{
			get
			{
				return (CDataElement)this["body"];
			}
			set
			{
				this["body"] = value;
			}
		}

	
		public bool IsDefined
		{
			get
			{
				return (!String.IsNullOrEmpty(this.Body.ToString()));
			}
		}
	}
}
