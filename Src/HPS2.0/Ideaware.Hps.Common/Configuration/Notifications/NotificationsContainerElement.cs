﻿using System.Configuration;

namespace Ideaware.Hps.Common.Configuration.Notifications
{
	public class NotificationsContainerElement : ConfigurationElement
	{
		[ConfigurationProperty("subscription", IsRequired = false)]
		public NotificationElement Subscription
		{
			get
			{
				return (NotificationElement)this["subscription"];
			}
			set
			{
				this["subscription"] = value;
			}
		}

		[ConfigurationProperty("membershipPasswordReset", IsRequired = false)]
		public NotificationElement MembershipPasswordReset
		{
			get
			{
				return (NotificationElement)this["membershipPasswordReset"];
			}
			set
			{
				this["membershipPasswordReset"] = value;
			}
		}
	}
}
