﻿using System;

namespace Ideaware.Hps.Common.Configuration
{
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class CDataConfigurationPropertyAttribute
		: Attribute
	{
	}
}
