﻿using System.Configuration;

namespace Ideaware.Hps.Common.Configuration
{
	public class TemplateElement : ConfigurationElement
	{
		[ConfigurationProperty("path", IsRequired = false)]
		public string Path
		{
			get
			{
				return (string)this["path"];
			}
			set
			{
				this["path"] = value;
			}
		}

		/// <summary>
		/// Gets the current master name.
		/// </summary>
		public string Master
		{
			get
			{
				return this.UseTemplates ? "Templated" : "_Layout";
			}
		}

		/// <summary>
		/// Gets the current Mobile master name.
		/// </summary>
		public string MobileMaster
		{
			get
			{
				return "Mobile";
			}
		}

		/// <summary>
		/// Determines if the application uses templates
		/// </summary>
		[ConfigurationProperty("useTemplates", IsRequired = true)]
		public bool UseTemplates
		{
			get
			{
				return (bool)this["useTemplates"];
			}
			set
			{
				this["useTemplates"] = value;
			}
		}

		/// <summary>
		/// Determines if the application uses mobile templates
		/// </summary>
		[ConfigurationProperty("mobile", IsRequired = false)]
		public TemplateMobileElement Mobile
		{
			get
			{
				return (TemplateMobileElement)this["mobile"];
			}
			set
			{
				this["mobile"] = value;
			}
		}
	}
}
