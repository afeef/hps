﻿using System;
using System.Configuration;

namespace Ideaware.Hps.Common.Configuration
{
	public class TemplateMobileElement : ConfigurationElement, IOptionalElement
	{
		/// <summary>
		/// User agent regular expression
		/// </summary>
		[ConfigurationProperty("regex", IsRequired = false)]
		public string Regex
		{
			get
			{
				return (string)this["regex"];
			}
			set
			{
				this["regex"] = value;
			}
		}

		#region IOptionalElement Members
		public bool IsDefined
		{
			get
			{
				return (!String.IsNullOrEmpty(this.Regex));
			}
		}
		#endregion
	}
}
