﻿using System.Configuration;

namespace Ideaware.Hps.Common.Configuration
{
	public class TopicElement : ConfigurationElement
	{
		[ConfigurationProperty("messagesPerPage", IsRequired = true)]
		public int MessagesPerPage
		{
			get
			{
				return (int)this["messagesPerPage"];
			}
			set
			{
				this["messagesPerPage"] = value;
			}
		}
	}
}
