﻿namespace Ideaware.Hps.Common.Validation
{
	public interface IEnsureValidation
	{
		void ValidateFields();
	}
}
