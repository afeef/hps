﻿using System;

namespace Ideaware.Hps.Common
{
	/// <summary>
	/// Represent a generic error on the authentication provider
	/// </summary>
	public class AuthenticationProviderException : Exception
	{
		/// <summary>
		/// Represent a generic error on the authentication provider
		/// </summary>
		public AuthenticationProviderException(string message)
			: base(message)
		{

		}
	}
}
