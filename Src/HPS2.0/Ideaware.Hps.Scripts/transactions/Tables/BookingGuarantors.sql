﻿CREATE TABLE [transactions].[BookingGuarantors] (
    [BookingGuarantorId] INT IDENTITY (1, 1) NOT NULL,
    [BookingId]          INT NULL,
    [GuarantorId]        INT NULL,
    CONSTRAINT [PK_BookingGuranter_1] PRIMARY KEY CLUSTERED ([BookingGuarantorId] ASC),
    CONSTRAINT [FK_BookingGuranter_Booking] FOREIGN KEY ([BookingId]) REFERENCES [transactions].[Booking] ([BookingId]),
    CONSTRAINT [FK_BookingGuranter_Users] FOREIGN KEY ([GuarantorId]) REFERENCES [security].[Users] ([UserId])
);

