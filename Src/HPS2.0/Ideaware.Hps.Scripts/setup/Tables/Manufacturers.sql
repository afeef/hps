﻿CREATE TABLE [setup].[Manufacturers] (
    [ManufacturerId]   INT           IDENTITY (1, 1) NOT NULL,
    [ManufacturerName] VARCHAR (150) NULL,
    [ContactPerson]    VARCHAR (150) NULL,
    [OfficeAddress]    VARCHAR (300) NULL,
    [ContactNumber]    VARCHAR (50)  NULL,
    [FaxNumber]        VARCHAR (50)  NULL,
    [EmailAddress]     VARCHAR (30)  NULL,
    [IsBlock]          BIT           NULL,
    [BlockDetail]      VARCHAR (300) NULL,
    [DateCreated]      DATETIME      NULL,
    [DateModified]     DATETIME      NULL,
    [IsActive]         BIT           NULL,
    [AccessUserId]     INT           NULL,
    CONSTRAINT [PK_Manufacturer] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC)
);

