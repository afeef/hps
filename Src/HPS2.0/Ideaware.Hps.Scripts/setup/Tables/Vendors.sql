﻿CREATE TABLE [setup].[Vendors] (
    [VendorId]      INT           IDENTITY (1, 1) NOT NULL,
    [VendorName]    VARCHAR (150) NULL,
    [ContactPerson] VARCHAR (150) NULL,
    [OfficeAddress] VARCHAR (300) NULL,
    [ContactNumber] VARCHAR (50)  NULL,
    [FaxNumber]     VARCHAR (50)  NULL,
    [EmailAddress]  VARCHAR (30)  NULL,
    [NTNNumber]     VARCHAR (30)  NULL,
    [IsBlock]       BIT           NULL,
    [BlockDetail]   VARCHAR (300) NULL,
    [DateCreated]   DATETIME      NULL,
    [DateModified]  DATETIME      NULL,
    [IsActive]      BIT           NULL,
    [AccessUserId]  INT           NULL,
    CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED ([VendorId] ASC)
);

