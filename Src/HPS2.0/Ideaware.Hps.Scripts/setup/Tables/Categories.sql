﻿CREATE TABLE [setup].[Categories] (
    [CategoryId]        INT           IDENTITY (1, 1) NOT NULL,
    [CategoryName]      VARCHAR (150) NULL,
    [Color]             BIT           NULL,
    [EngineNo]          BIT           NULL,
    [ChasisNo]          BIT           NULL,
    [RegistrationNo]    BIT           NULL,
    [ProductSize]       BIT           NULL,
    [ProductWeight]     BIT           NULL,
    [EMINo]             BIT           NULL,
    [ManufacturingDate] BIT           NULL,
    [ValidityDate]      BIT           NULL,
    [KeyNo]             BIT           NULL,
    [DateCreated]       DATETIME      NULL,
    [DateModified]      DATETIME      NULL,
    [IsActive]          BIT           NULL,
    [AccessUserId]      INT           NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
);

