﻿CREATE TABLE [setup].[Tenants] (
    [TenantId]          INT            IDENTITY (1, 1) NOT NULL,
    [TenantName]        NVARCHAR (50)  NULL,
    [TenantDescription] NVARCHAR (256) NULL,
    [DateCreated]       DATETIME       NULL,
    [DateModified]      DATETIME       NULL,
    [IsActive]          BIT            NULL,
    [AccessUserId]      INT            NULL,
    CONSTRAINT [PK_Tenants] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

