﻿CREATE TABLE [setup].[Colors] (
    [ColorId]      INT           IDENTITY (1, 1) NOT NULL,
    [ColorName]    VARCHAR (150) NULL,
    [DateCreated]  DATETIME      NULL,
    [DateModified] DATETIME      NULL,
    [IsActive]     BIT           NULL,
    [AccessUserId] INT           NULL,
    CONSTRAINT [PK_Color] PRIMARY KEY CLUSTERED ([ColorId] ASC)
);

