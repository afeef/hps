﻿CREATE TABLE [setup].[Models] (
    [ModelId]        INT           IDENTITY (1, 1) NOT NULL,
    [CategoryId]     INT           NULL,
    [ManufacturerId] INT           NULL,
    [ModelName]      VARCHAR (150) NULL,
    [DateCreated]    DATETIME      NULL,
    [DateModified]   DATETIME      NULL,
    [IsActive]       BIT           NULL,
    [AccessUserId]   INT           NULL,
    CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED ([ModelId] ASC)
);

