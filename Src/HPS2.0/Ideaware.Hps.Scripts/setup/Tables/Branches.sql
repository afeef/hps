﻿CREATE TABLE [setup].[Branches] (
    [BranchId]      INT             IDENTITY (1, 1) NOT NULL,
    [BranchName]    VARCHAR (150)   NULL,
    [OfficeAddress] VARCHAR (300)   NULL,
    [ContactNumber] VARCHAR (50)    NULL,
    [FaxNumber]     VARCHAR (50)    NULL,
    [EmailAddress]  VARCHAR (30)    NULL,
    [IsBlock]       BIT             NULL,
    [BlockDetail]   VARCHAR (300)   NULL,
    [Lattitude]     DECIMAL (10, 6) NULL,
    [Longitude]     DECIMAL (10, 6) NULL,
    [DateCreated]   DATETIME        NULL,
    [DateModified]  DATETIME        NULL,
    [IsActive]      BIT             NULL,
    [AccessUserId]  INT             NULL,
    CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED ([BranchId] ASC)
);

