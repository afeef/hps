﻿CREATE FUNCTION [dbo].[fnc_GetColumnsForValueByCommas]  
( 
 -- Add the parameters for the function here 
 @schemaName  VARCHAR(50), 
 @tableName  VARCHAR(50) 
) 
RETURNS VARCHAR(4000) 
AS 
BEGIN 
  
 DECLARE @column VARCHAR(4000) 
 ,@typeName VARCHAR(500) 
 ,@columnS VARCHAR(2000) 
 ,@ColStart VARCHAR(50) 
 ,@ColEnd VARCHAR(50) 
 ,@i INT 

SET @i=0 
 
 SET @column='' 
 SET @columnS='' 
 
DECLARE Cur_Columns CURSOR STATIC FOR 
  
 SELECT sys.columns.name,sys.types.name FROM sys.schemas  
 INNER JOIN sys.objects ON sys.schemas.schema_id=sys.objects.schema_id  
  INNER JOIN sys.columns ON sys.columns.object_id = sys.objects.object_id  
   INNER JOIN sys.types ON sys.columns.user_type_id = sys.types.user_type_id  
    WHERE sys.types.name <>'timestamp' AND sys.objects.type ='U' AND sys.objects.name = @tableName AND sys.schemas.name = @schemaName 
     ORDER BY sys.columns.column_id 
 

 OPEN Cur_Columns 
  FETCH FIRST FROM Cur_Columns INTO @column,@typeName 
  WHILE @@FETCH_STATUS = 0 
   BEGIN 
    IF  @typeName = 'text' OR @typeName = 'uniqueidentifier' OR  @typeName = 'varbinary' OR @typeName = 'smalldatetime'OR @typeName = 'char' OR @typeName = 'datetime' OR @typeName = 'varchar'    
    BEGIN 
      SET @ColStart = ' ISNULL(CHAR(39) + CAST ( '   
      SET @ColEnd = ' AS VARCHAR(MAX))+ CHAR(39),''NULL'') ' 
     END 
    ELSE IF  @typeName = 'nvarchar'OR  @typeName = 'ntext' OR @typeName = 'nchar'   
     BEGIN 
      SET @ColStart = 'ISNULL( ''N''+CHAR(39)+ CAST ( '  
      SET @ColEnd = ' AS NVARCHAR(MAX)) + CHAR(39),''NULL'') ' 
     END 
    ELSE  
     BEGIN  
      SET @ColStart = 'ISNULL(CAST ( '  
      SET @ColEnd = ' AS VARCHAR(MAX)), ''NULL'') ' 
     END 

   IF @i=0 
     SET @columnS=@ColStart+@column+@ColEnd 
    ELSE 
     SET @columnS=@columnS +'+'',''+'+ @ColStart+@column+@ColEnd 
     SET @i=@i+1 
 
    FETCH NEXT FROM Cur_Columns INTO @column,@typeName 
   END 

  CLOSE Cur_Columns 
  DEALLOCATE Cur_Columns 
 

-- Return the result of the function 
 RETURN @columns 
 
END  