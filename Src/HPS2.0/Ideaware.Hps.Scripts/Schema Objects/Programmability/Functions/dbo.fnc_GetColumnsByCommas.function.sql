﻿CREATE FUNCTION [dbo].[fnc_GetColumnsByCommas]  
( 
 -- Add the parameters for the function
 @schemaName  VARCHAR(50), 
 @tableName  VARCHAR(50) 
) 
RETURNS VARCHAR(4000) 

AS 

BEGIN 

 DECLARE @column VARCHAR(2000) 
 ,@columnS VARCHAR(4000) 
 ,@i INT 
 SET @i=0 
 SET @column='' 
 SET @columnS='' 

 DECLARE Cur_Columns CURSOR STATIC FOR 

 SELECT sys.columns.name FROM sys.schemas   
 INNER JOIN sys.objects ON sys.schemas.schema_id=sys.objects.schema_id  
  INNER JOIN sys.columns ON sys.columns.object_id = sys.objects.object_id  
   INNER JOIN sys.types ON sys.columns.user_type_id = sys.types.user_type_id  
    WHERE sys.types.name <>'timestamp' AND sys.objects.type ='U' AND sys.objects.name = @tableName AND sys.schemas.name = @schemaName 
     ORDER BY sys.columns.column_id 
OPEN Cur_Columns 
  FETCH FIRST FROM Cur_Columns INTO @column 
  WHILE @@FETCH_STATUS = 0 
   BEGIN 
    IF @i=0 
     SET @columnS='['+@column+']' 
    ELSE 
     SET @columnS=@columnS +','+ '['+@column+']' 
     SET @i=@i+1 
 
    FETCH NEXT FROM Cur_Columns INTO @column 
   END 
 
  CLOSE Cur_Columns 
  DEALLOCATE Cur_Columns 
 
 -- Return the result of the function 
 RETURN @columns 
 
END  