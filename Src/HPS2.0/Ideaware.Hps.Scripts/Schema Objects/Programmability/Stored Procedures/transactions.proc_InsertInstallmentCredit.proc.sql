﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertInstallmentCredit] - Insert Procedure Script for InstallmentCredit
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_InsertInstallmentCredit]
(
	@InstallmentCreditId uniqueidentifier,
	@BranchId uniqueidentifier = NULL,
	@BookingId uniqueidentifier = NULL,
	@InstallmentPlanId uniqueidentifier = NULL,
	@ReceiptNo varchar(50) = NULL,
	@PaymentDate datetime = NULL,
	@AmountPaid int = NULL,
	@FinePaid int = NULL,
	@ModeOfPayment varchar(50) = NULL,
	@ChequeDetail varchar(150) = NULL,
	@InstallmentRemarks varchar(300) = NULL,
	@IsActive bit,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Transactions].[InstallmentCredit]
	(
		[InstallmentCreditId],
		[BranchId],
		[BookingId],
		[InstallmentPlanId],
		[ReceiptNo],
		[PaymentDate],
		[AmountPaid],
		[FinePaid],
		[ModeOfPayment],
		[ChequeDetail],
		[InstallmentRemarks],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@InstallmentCreditId,
		@BranchId,
		@BookingId,
		@InstallmentPlanId,
		@ReceiptNo,
		@PaymentDate,
		@AmountPaid,
		@FinePaid,
		@ModeOfPayment,
		@ChequeDetail,
		@InstallmentRemarks,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
