﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertManufacturer] - Insert Procedure Script for Manufacturer
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertManufacturer]
(
	@ManufacturerId uniqueidentifier,
	@ManufacturerName varchar(150) = NULL,
	@ContactPerson varchar(150) = NULL,
	@OfficeAddress varchar(300) = NULL,
	@ContactNumber varchar(50) = NULL,
	@FaxNumber varchar(50) = NULL,
	@EmailAddress varchar(30) = NULL,
	@IsBlock bit = NULL,
	@BlockDetail varchar(300) = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[Manufacturer]
	(
		[ManufacturerId],
		[ManufacturerName],
		[ContactPerson],
		[OfficeAddress],
		[ContactNumber],
		[FaxNumber],
		[EmailAddress],
		[IsBlock],
		[BlockDetail],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@ManufacturerId,
		@ManufacturerName,
		@ContactPerson,
		@OfficeAddress,
		@ContactNumber,
		@FaxNumber,
		@EmailAddress,
		@IsBlock,
		@BlockDetail,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
