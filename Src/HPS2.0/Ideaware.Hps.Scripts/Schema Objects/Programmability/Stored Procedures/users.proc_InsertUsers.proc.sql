﻿


--|--------------------------------------------------------------------------------
--| [proc_InsertUsers] - Insert Procedure Script for Users
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertUsers]
(
	@UserId uniqueidentifier,
	@PersonId uniqueidentifier = NULL,
	@UserName nvarchar(256),
	@Password nvarchar(128),
	@Email nvarchar(256) = NULL,
	@PasswordQuestion nvarchar(256) = NULL,
	@PasswordAnswer nvarchar(256) = NULL,
	@IsActive bit = NULL,	
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[Users]
	(
		[UserId],
		[PersonId],
		[UserName],
		[Password],
		[Email],
		[PasswordQuestion],
		[PasswordAnswer],
		[IsActive],		
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@UserId,
		@PersonId,
		@UserName,
		@Password,
		@Email,
		@PasswordQuestion,
		@PasswordAnswer,
		@IsActive,		
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error


