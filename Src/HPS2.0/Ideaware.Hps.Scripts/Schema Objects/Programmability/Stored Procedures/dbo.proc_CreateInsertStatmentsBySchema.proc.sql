﻿

CREATE PROCEDURE [dbo].[proc_CreateInsertStatmentsBySchema]
(
      -- Add the parameters for the function here
      @schemaName VARCHAR(50)
)

AS
BEGIN


     CREATE TABLE #Tables
(
      id    INT IDENTITY,
      tbname      VARCHAR(100),
      columns VARCHAR(4000),
      columnsForVal VARCHAR(4000)
)


INSERT INTO #Tables (tbname,columns,columnsForVal)

SELECT '['+sys.schemas.name+'].['+sys.objects.name+']', dbo.fnc_GetColumnsByCommas(@schemaName,sys.objects.name),dbo.fnc_GetColumnsForValueByCommas(@schemaName,sys.objects.name)  FROM sys.schemas
            INNER JOIN sys.objects ON sys.schemas.schema_id=sys.objects.schema_id
                  WHERE sys.objects.type ='U'
                              AND sys.schemas.name = @schemaName

--SELECT * FROM #Tables
DECLARE @tbID INT
      ,@tbNAME VARCHAR(50)
      ,@tbColumn VARCHAR (4000)
      ,@tbColumnforVal VARCHAR (4000)
      ,@SQLstr VARCHAR(4000)
      ,@label VARCHAR(500)
      ,@Count INT

SELECT @Count = count(*) FROM #Tables

SET @tbID = 1
      WHILE @tbID <> @Count +1
            BEGIN
                  SELECT @tbNAME = tbNAME FROM #Tables WHERE id = @tbID
                  SELECT @tbColumn =  columns FROM #Tables WHERE id = @tbID
                  SELECT @tbColumnforVal =  columnsForVal FROM #Tables WHERE id = @tbID
                  SET @label = '--Table: '+@tbNAME
                  SET @SQLstr ='SELECT '+ CHAR(39)+'INSERT INTO '+@tbNAME+'  ('+@tbColumn+')  VALUES ( ''+'+@tbColumnforVal+' +'')'' FROM '+@tbNAME

                  EXEC(@SQLstr)
                  SET @tbID = @tbID + 1
             
            END


DROP TABLE #Tables

END