﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertGoodsTransferProduct] - Insert Procedure Script for GoodsTransferProduct
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_InsertGoodsTransferProduct]
(
	@GoodsTransferProductId uniqueidentifier,
	@GoodsTransferId uniqueidentifier = NULL,
	@ProductId uniqueidentifier = NULL,
	@IsActive bit,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Stock].[GoodsTransferProduct]
	(
		[GoodsTransferProductId],
		[GoodsTransferId],
		[ProductId],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@GoodsTransferProductId,
		@GoodsTransferId,
		@ProductId,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
