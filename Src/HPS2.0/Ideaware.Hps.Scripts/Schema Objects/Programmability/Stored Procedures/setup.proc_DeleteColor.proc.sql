﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteColor] - Delete Procedure Script for Color
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeleteColor]
(
	@ColorId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[Color]
	WHERE  
		[ColorId] = @ColorId

	RETURN @@Error
