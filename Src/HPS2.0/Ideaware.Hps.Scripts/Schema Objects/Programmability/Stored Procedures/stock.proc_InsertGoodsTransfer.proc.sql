﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertGoodsTransfer] - Insert Procedure Script for GoodsTransfer
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_InsertGoodsTransfer]
(
	@GoodsTransferId uniqueidentifier,
	@GoodsTransferNo varchar(50) = NULL,
	@GoodsTransferDate datetime = NULL,
	@GoodsTransferFrom int = NULL,
	@GoodsTransferTo int = NULL,
	@GoodsTransferRemarks varchar(500) = NULL,
	@IsActive bit,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Stock].[GoodsTransfer]
	(
		[GoodsTransferId],
		[GoodsTransferNo],
		[GoodsTransferDate],
		[GoodsTransferFrom],
		[GoodsTransferTo],
		[GoodsTransferRemarks],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@GoodsTransferId,
		@GoodsTransferNo,
		@GoodsTransferDate,
		@GoodsTransferFrom,
		@GoodsTransferTo,
		@GoodsTransferRemarks,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
