﻿
--|--------------------------------------------------------------------------------
--| [proc_DeletePersonType] - Delete Procedure Script for PersonType
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeletePersonType]
(
	@PersonTypeId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[PersonType]
	WHERE  
		[PersonTypeId] = @PersonTypeId

	RETURN @@Error
