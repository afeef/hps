﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteBranchUser] - Delete Procedure Script for BranchUser
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteBranchUser]
(
	@BranchUserId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[BranchUser]
	WHERE  
		[BranchUserId] = @BranchUserId

	RETURN @@Error
