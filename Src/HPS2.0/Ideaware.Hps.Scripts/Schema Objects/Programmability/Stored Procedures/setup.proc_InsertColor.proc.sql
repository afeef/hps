﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertColor] - Insert Procedure Script for Color
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertColor]
(
	@ColorId uniqueidentifier,
	@ColorName varchar(150) = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[Color]
	(
		[ColorId],
		[ColorName],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@ColorId,
		@ColorName,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
