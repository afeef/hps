﻿
--|--------------------------------------------------------------------------------
--| [proc_DeletePerson] - Delete Procedure Script for Person
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeletePerson]
(
	@PersonId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[Person]
	WHERE  
		[PersonId] = @PersonId

	RETURN @@Error
