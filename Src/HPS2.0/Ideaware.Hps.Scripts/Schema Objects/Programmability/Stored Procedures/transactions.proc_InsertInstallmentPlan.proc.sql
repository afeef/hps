﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertInstallmentPlan] - Insert Procedure Script for InstallmentPlan
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_InsertInstallmentPlan]
(
	@InstallmentPlanId uniqueidentifier,
	@BookingId uniqueidentifier = NULL,
	@ParentId uniqueidentifier = NULL,
	@InstallmentNo int = NULL,
	@InstallmentDate datetime = NULL,
	@InstallmentAmount int = NULL,
	@ProfitAmount int = NULL,
	@IsComplete bit = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Transactions].[InstallmentPlan]
	(
		[InstallmentPlanId],
		[BookingId],
		[ParentId],
		[InstallmentNo],
		[InstallmentDate],
		[InstallmentAmount],
		[ProfitAmount],
		[IsComplete],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@InstallmentPlanId,
		@BookingId,
		@ParentId,
		@InstallmentNo,
		@InstallmentDate,
		@InstallmentAmount,
		@ProfitAmount,
		@IsComplete,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
