﻿

--|--------------------------------------------------------------------------------
--| [proc_DeleteUserRoles] - Delete Procedure Script for UserRoles
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeletePersonRoles]
(
	@UserId uniqueidentifier,
	@RoleId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[PersonRoles]
	WHERE  
		[PersonId] = @UserId AND
		[RoleId] = @RoleId

	RETURN @@Error

