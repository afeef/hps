﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertDocumentIssuance] - Insert Procedure Script for DocumentIssuance
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_InsertDocumentIssuance]
(
	@DocumentIssuanceId uniqueidentifier,
	@BranchId uniqueidentifier = NULL,
	@DocumentTypeId uniqueidentifier = NULL,
	@IssuanceDate datetime = NULL,
	@DocumentNo varchar(100) = NULL,
	@CopyStatus int = NULL,
	@NumberOfCopies int = NULL,
	@ReceivingPerson varchar(100) = NULL,
	@IssuedBy varchar(100) = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Transactions].[DocumentIssuance]
	(
		[DocumentIssuanceId],
		[BranchId],
		[DocumentTypeId],
		[IssuanceDate],
		[DocumentNo],
		[CopyStatus],
		[NumberOfCopies],
		[ReceivingPerson],
		[IssuedBy],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@DocumentIssuanceId,
		@BranchId,
		@DocumentTypeId,
		@IssuanceDate,
		@DocumentNo,
		@CopyStatus,
		@NumberOfCopies,
		@ReceivingPerson,
		@IssuedBy,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
