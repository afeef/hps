﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [stock].[proc_GetGoodsTransferProductById]
	-- Add the parameters for the stored procedure here
	@Key	UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM [Stock].[GoodsTransferProduct] WHERE [Stock].[GoodsTransferProduct].[GoodsTransferProductId] = @Key
END

