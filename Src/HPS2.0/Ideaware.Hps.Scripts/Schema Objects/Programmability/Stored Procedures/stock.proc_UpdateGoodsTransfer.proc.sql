﻿
--|--------------------------------------------------------------------------------
--| [proc_UpdateGoodsTransfer] - Update Procedure Script for GoodsTransfer
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_UpdateGoodsTransfer]
(
	@UpdateSet	NVARCHAR(4000),
	@Key UNIQUEIDENTIFIER,
	@TimeStampVal TIMESTAMP
)
AS

	SET NOCOUNT ON
	DECLARE @sql VARCHAR(4000)
	Declare @table_time_stamp as timestamp
-- Get existing TimeStamp	
	SELECT	@table_time_stamp = TimeStampVal 
	FROM [Stock].[GoodsTransfer]
	WHERE 
		[GoodsTransferId] = @Key	

-- Update selective columns	if timestamp is equal to existing timestamp (to avoid concurrency problems)
	IF @table_time_stamp = @TimeStampVal
	BEGIN
		SET @sql = 'UPDATE [Stock].[GoodsTransfer] SET ' + @UpdateSet + ' WHERE 
				[GoodsTransferId] ='''+ CAST(@Key AS VARCHAR(38))+''''

		EXEC(@sql)
	END
	ELSE
	BEGIN
		RAISERROR('Concurrency Violation', 25, 1)
	END

