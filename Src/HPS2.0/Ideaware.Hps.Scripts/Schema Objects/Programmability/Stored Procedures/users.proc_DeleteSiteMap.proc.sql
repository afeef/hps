﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteSiteMap] - Delete Procedure Script for SiteMap
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteSiteMap]
(
	@SiteMapId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[SiteMap]
	WHERE  
		[SiteMapId] = @SiteMapId

	RETURN @@Error
