﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteInstallmentCredit] - Delete Procedure Script for InstallmentCredit
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_DeleteInstallmentCredit]
(
	@InstallmentCreditId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Transactions].[InstallmentCredit]
	WHERE  
		[InstallmentCreditId] = @InstallmentCreditId

	RETURN @@Error
