﻿--|--------------------------------------------------------------------------------
--| [proc_UpdateBookingGuranter] - Update Procedure Script for BookingGuranter
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_UpdateBookingGuranter]
(
	@UpdateSet	NVARCHAR(4000),
	@Key UNIQUEIDENTIFIER,
	@TimeStampVal TIMESTAMP
)
AS

	SET NOCOUNT ON
	DECLARE @sql VARCHAR(4000)
	Declare @table_time_stamp as timestamp
-- Get existing TimeStamp	
	SELECT	@table_time_stamp = TimeStampVal 
	FROM [Transactions].[BookingGuranter]
	WHERE 
		[BookingGuaranterId] = @Key	

-- Update selective columns	if timestamp is equal to existing timestamp (to avoid concurrency problems)
	IF @table_time_stamp = @TimeStampVal
	BEGIN
		SET @sql = 'UPDATE [Transactions].[BookingGuranter] SET ' + @UpdateSet + ' WHERE 
				[BookingGuaranterId] ='''+ CAST(@Key AS VARCHAR(38))+''''

		EXEC(@sql)
	END
	ELSE
	BEGIN
		RAISERROR('Concurrency Violation', 25, 1)
	END

