﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteDocumentIssuance] - Delete Procedure Script for DocumentIssuance
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_DeleteDocumentIssuance]
(
	@DocumentIssuanceId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Transactions].[DocumentIssuance]
	WHERE  
		[DocumentIssuanceId] = @DocumentIssuanceId

	RETURN @@Error
