﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteBranch] - Delete Procedure Script for Branch
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeleteBranch]
(
	@BranchId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[Branch]
	WHERE  
		[BranchId] = @BranchId

	RETURN @@Error
