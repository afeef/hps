﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteUsers] - Delete Procedure Script for Users
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteUsers]
(
	@UserId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[Users]
	WHERE  
		[UserId] = @UserId

	RETURN @@Error
