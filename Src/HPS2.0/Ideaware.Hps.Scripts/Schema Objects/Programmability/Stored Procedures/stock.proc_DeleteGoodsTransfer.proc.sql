﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteGoodsTransfer] - Delete Procedure Script for GoodsTransfer
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_DeleteGoodsTransfer]
(
	@GoodsTransferId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Stock].[GoodsTransfer]
	WHERE  
		[GoodsTransferId] = @GoodsTransferId

	RETURN @@Error
