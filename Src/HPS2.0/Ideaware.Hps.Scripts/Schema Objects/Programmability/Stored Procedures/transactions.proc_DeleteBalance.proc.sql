﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteBalance] - Delete Procedure Script for Balance
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_DeleteBalance]
(
	@BalanceId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Transactions].[Balance]
	WHERE  
		[BalanceId] = @BalanceId

	RETURN @@Error
