﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteInstallmentPlan] - Delete Procedure Script for InstallmentPlan
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_DeleteInstallmentPlan]
(
	@InstallmentPlanId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Transactions].[InstallmentPlan]
	WHERE  
		[InstallmentPlanId] = @InstallmentPlanId

	RETURN @@Error
