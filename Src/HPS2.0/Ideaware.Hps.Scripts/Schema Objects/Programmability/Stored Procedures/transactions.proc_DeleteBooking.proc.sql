﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteBooking] - Delete Procedure Script for Booking
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_DeleteBooking]
(
	@BookingId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Transactions].[Booking]
	WHERE  
		[BookingId] = @BookingId

	RETURN @@Error
