﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteModules] - Delete Procedure Script for Modules
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteModules]
(
	@ModuleId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[Modules]
	WHERE  
		[ModuleId] = @ModuleId

	RETURN @@Error
