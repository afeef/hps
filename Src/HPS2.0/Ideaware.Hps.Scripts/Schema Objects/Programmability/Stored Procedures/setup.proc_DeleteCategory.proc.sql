﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteCategory] - Delete Procedure Script for Category
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeleteCategory]
(
	@CategoryId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[Category]
	WHERE  
		[CategoryId] = @CategoryId

	RETURN @@Error
