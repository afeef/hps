﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteProduct] - Delete Procedure Script for Product
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_DeleteProduct]
(
	@ProductId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Stock].[Product]
	WHERE  
		[ProductId] = @ProductId

	RETURN @@Error
