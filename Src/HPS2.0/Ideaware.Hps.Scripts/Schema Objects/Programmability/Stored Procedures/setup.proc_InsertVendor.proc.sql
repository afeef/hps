﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertVendor] - Insert Procedure Script for Vendor
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertVendor]
(
	@VendorId uniqueidentifier,
	@VendorName varchar(150) = NULL,
	@ContactPerson varchar(150) = NULL,
	@OfficeAddress varchar(300) = NULL,
	@ContactNumber varchar(50) = NULL,
	@FaxNumber varchar(50) = NULL,
	@EmailAddress varchar(30) = NULL,
	@NTNNumber varchar(30) = NULL,
	@IsBlock bit = NULL,
	@BlockDetail varchar(300) = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[Vendor]
	(
		[VendorId],
		[VendorName],
		[ContactPerson],
		[OfficeAddress],
		[ContactNumber],
		[FaxNumber],
		[EmailAddress],
		[NTNNumber],
		[IsBlock],
		[BlockDetail],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@VendorId,
		@VendorName,
		@ContactPerson,
		@OfficeAddress,
		@ContactNumber,
		@FaxNumber,
		@EmailAddress,
		@NTNNumber,
		@IsBlock,
		@BlockDetail,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
