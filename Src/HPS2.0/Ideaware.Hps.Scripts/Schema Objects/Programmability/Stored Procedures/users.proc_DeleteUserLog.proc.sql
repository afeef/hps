﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteUserLog] - Delete Procedure Script for UserLog
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteUserLog]
(
	@UserLogId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[UserLog]
	WHERE  
		[UserLogId] = @UserLogId

	RETURN @@Error
