﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertProduct] - Insert Procedure Script for Product
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_InsertProduct]
(
	@ProductId uniqueidentifier,
	@GoodsReceivedId uniqueidentifier = NULL,
	@ManufacturerId uniqueidentifier = NULL,
	@ModelId uniqueidentifier = NULL,
	@BranchId uniqueidentifier = NULL,
	@ColorId uniqueidentifier = NULL,
	@ProductNo varchar(50) = NULL,
	@ProductName varchar(300) = NULL,
	@EngineNo varchar(100) = NULL,
	@ChasisNo varchar(100) = NULL,
	@RegistrationNo varchar(15) = NULL,
	@ProductSize varchar(50) = NULL,
	@ProductWeight varchar(50) = NULL,
	@EMINo varchar(50) = NULL,
	@ManufacturingDate datetime = NULL,
	@ValidityDate datetime = NULL,
	@KeyNo varchar(100) = NULL,
	@ActualPrice int = NULL,
	@SalePrice int = NULL,
	@ProductStatus int = NULL,
	@ProductRemarks varchar(500) = NULL,
	@IsActive bit,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Stock].[Product]
	(
		[ProductId],
		[GoodsReceivedId],
		[ManufacturerId],
		[ModelId],
		[BranchId],
		[ColorId],
		[ProductNo],
		[ProductName],
		[EngineNo],
		[ChasisNo],
		[RegistrationNo],
		[ProductSize],
		[ProductWeight],
		[EMINo],
		[ManufacturingDate],
		[ValidityDate],
		[KeyNo],
		[ActualPrice],
		[SalePrice],
		[ProductStatus],
		[ProductRemarks],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@ProductId,
		@GoodsReceivedId,
		@ManufacturerId,
		@ModelId,
		@BranchId,
		@ColorId,
		@ProductNo,
		@ProductName,
		@EngineNo,
		@ChasisNo,
		@RegistrationNo,
		@ProductSize,
		@ProductWeight,
		@EMINo,
		@ManufacturingDate,
		@ValidityDate,
		@KeyNo,
		@ActualPrice,
		@SalePrice,
		@ProductStatus,
		@ProductRemarks,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
