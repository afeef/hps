﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertModules] - Insert Procedure Script for Modules
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertModules]
(
	@ModuleId uniqueidentifier,
	@Description nvarchar(50) = NULL,
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[Modules]
	(
		[ModuleId],
		[Description],
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@ModuleId,
		@Description,
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error
