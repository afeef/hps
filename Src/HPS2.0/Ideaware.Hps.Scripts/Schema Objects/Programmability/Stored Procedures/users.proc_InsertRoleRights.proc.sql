﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertRoleRights] - Insert Procedure Script for RoleRights
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertRoleRights]
(
	@RoleRightId uniqueidentifier,
	@SiteMapId uniqueidentifier = NULL,
	@RoleId uniqueidentifier = NULL,
	@HasViewRight bit = NULL,
	@HasInsertRight bit = NULL,
	@HasUpdateRight bit = NULL,
	@HasDeleteRight bit = NULL,
	@HasPrintRight bit = NULL,
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[RoleRights]
	(
		[RoleRightId],
		[SiteMapId],
		[RoleId],
		[HasViewRight],
		[HasInsertRight],
		[HasUpdateRight],
		[HasDeleteRight],
		[HasPrintRight],
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@RoleRightId,
		@SiteMapId,
		@RoleId,
		@HasViewRight,
		@HasInsertRight,
		@HasUpdateRight,
		@HasDeleteRight,
		@HasPrintRight,
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error
