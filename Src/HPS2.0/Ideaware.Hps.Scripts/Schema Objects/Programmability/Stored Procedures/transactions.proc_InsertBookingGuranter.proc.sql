﻿--|--------------------------------------------------------------------------------
--| [proc_InsertBookingGuranter] - Insert Procedure Script for BookingGuranter
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_InsertBookingGuranter]
(
	@BookingGuaranterId uniqueidentifier,
	@BookingId uniqueidentifier = NULL,
	@GuaranterId uniqueidentifier = NULL,
	@IsActive bit = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Transactions].[BookingGuranter]
	(
		[BookingGuaranterId],
		[BookingId],
		[GuaranterId],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@BookingGuaranterId,
		@BookingId,
		@GuaranterId,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
