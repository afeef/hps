﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertRoles] - Insert Procedure Script for Roles
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertRoles]
(
	@RoleId uniqueidentifier,
	@RoleName nvarchar(256),
	@LoweredRoleName nvarchar(256),
	@Description nvarchar(256) = NULL,
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[Roles]
	(
		[RoleId],
		[RoleName],
		[LoweredRoleName],
		[Description],
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@RoleId,
		@RoleName,
		@LoweredRoleName,
		@Description,
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error
