﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteVendor] - Delete Procedure Script for Vendor
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeleteVendor]
(
	@VendorId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[Vendor]
	WHERE  
		[VendorId] = @VendorId

	RETURN @@Error
