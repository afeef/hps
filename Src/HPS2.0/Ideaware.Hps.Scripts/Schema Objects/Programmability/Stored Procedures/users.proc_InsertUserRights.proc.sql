﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertUserRights] - Insert Procedure Script for UserRights
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertUserRights]
(
	@UserRightId uniqueidentifier,
	@SiteMapId uniqueidentifier = NULL,
	@UserId uniqueidentifier = NULL,
	@HasViewRight bit = NULL,
	@HasInsertRight bit = NULL,
	@HasUpdateRight bit = NULL,
	@HasDeleteRight bit = NULL,
	@HasPrintRight bit = NULL,
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[UserRights]
	(
		[UserRightId],
		[SiteMapId],
		[UserId],
		[HasViewRight],
		[HasInsertRight],
		[HasUpdateRight],
		[HasDeleteRight],
		[HasPrintRight],
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@UserRightId,
		@SiteMapId,
		@UserId,
		@HasViewRight,
		@HasInsertRight,
		@HasUpdateRight,
		@HasDeleteRight,
		@HasPrintRight,
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error
