﻿


--|--------------------------------------------------------------------------------
--| [proc_InsertPerson] - Insert Procedure Script for Person
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertPerson]
(
	@PersonId uniqueidentifier,	
	@PersonName varchar(150) = NULL,
	@FatherName varchar(150) = NULL,
	@CNIC varchar(30) = NULL,
	@Occupation varchar(50) = NULL,
	@Designation varchar(50) = NULL,
	@ResidentialAddress varchar(250) = NULL,
	@City varchar(50) = NULL,
	@OfficeAddress varchar(250) = NULL,
	@MonthlyIncome varchar(250) = NULL,
	@OfficePhone varchar(50) = NULL,
	@ResidentialPhone varchar(50) = NULL,
	@CellNo varchar(20) = NULL,
	@FaxNo varchar(20) = NULL,
	@MultiGuarantee bit,
	@IsActive bit,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[Person]
	(
		[PersonId],		
		[PersonName],
		[FatherName],
		[CNIC],
		[Occupation],
		[Designation],
		[ResidentialAddress],
		[City],
		[OfficeAddress],
		[MonthlyIncome],
		[OfficePhone],
		[ResidentialPhone],
		[CellNo],
		[FaxNo],
		[MultiGuarantee],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@PersonId,		
		@PersonName,
		@FatherName,
		@CNIC,
		@Occupation,
		@Designation,
		@ResidentialAddress,
		@City,
		@OfficeAddress,
		@MonthlyIncome,
		@OfficePhone,
		@ResidentialPhone,
		@CellNo,
		@FaxNo,
		@MultiGuarantee,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error


