﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertModel] - Insert Procedure Script for Model
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertModel]
(
	@ModelId uniqueidentifier,
	@CategoryId int = NULL,
	@ManufacturerId int = NULL,
	@ModelName varchar(150) = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[Model]
	(
		[ModelId],
		[CategoryId],
		[ManufacturerId],
		[ModelName],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@ModelId,
		@CategoryId,
		@ManufacturerId,
		@ModelName,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
