﻿

--|--------------------------------------------------------------------------------
--| [proc_InsertUserRoles] - Insert Procedure Script for UserRoles
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertPersonRoles]
(
	@PersonId uniqueidentifier,
	@RoleId uniqueidentifier,
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[PersonRoles]
	(
		[PersonId],
		[RoleId],
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@PersonId,
		@RoleId,
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error

