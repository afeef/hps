﻿


--|--------------------------------------------------------------------------------
--| [proc_InsertBranchUser] - Insert Procedure Script for BranchUser
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertBranchUser]
(
	@BranchUserId uniqueidentifier,
	@BranchId uniqueidentifier = NULL,
	@PersonId uniqueidentifier = NULL,
	@RowGuid uniqueidentifier = NULL,
	@IsActive bit = NULL,	
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[BranchUser]
	(
		[BranchUserId],
		[BranchId],
		[PersonId],
		[RowGuid],
		[IsActive],		
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@BranchUserId,
		@BranchId,
		@PersonId,
		@RowGuid,
		@IsActive,		
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error


