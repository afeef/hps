﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteUserModules] - Delete Procedure Script for UserModules
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteUserModules]
(
	@UserModuleId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[UserModules]
	WHERE  
		[UserModuleId] = @UserModuleId

	RETURN @@Error
