﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertUserLog] - Insert Procedure Script for UserLog
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertUserLog]
(
	@UserLogId uniqueidentifier,
	@RowGuid uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[UserLog]
	(
		[UserLogId],
		[RowGuid],
		[AccessDateTime],
		[AccessUserId]
	)
	VALUES
	(
		@UserLogId,
		@RowGuid,
		@AccessDateTime,
		@AccessUserId
	)
	RETURN @@Error
