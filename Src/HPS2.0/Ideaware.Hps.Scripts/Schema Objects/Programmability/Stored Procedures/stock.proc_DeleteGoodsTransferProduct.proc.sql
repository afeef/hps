﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteGoodsTransferProduct] - Delete Procedure Script for GoodsTransferProduct
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_DeleteGoodsTransferProduct]
(
	@GoodsTransferProductId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Stock].[GoodsTransferProduct]
	WHERE  
		[GoodsTransferProductId] = @GoodsTransferProductId

	RETURN @@Error
