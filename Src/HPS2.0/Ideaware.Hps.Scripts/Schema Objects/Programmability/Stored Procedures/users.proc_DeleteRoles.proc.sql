﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteRoles] - Delete Procedure Script for Roles
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteRoles]
(
	@RoleId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[Roles]
	WHERE  
		[RoleId] = @RoleId

	RETURN @@Error
