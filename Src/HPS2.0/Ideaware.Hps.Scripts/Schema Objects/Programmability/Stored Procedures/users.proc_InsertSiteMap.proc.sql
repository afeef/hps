﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertSiteMap] - Insert Procedure Script for SiteMap
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertSiteMap]
(
	@SiteMapId uniqueidentifier,
	@ModuleId uniqueidentifier,
	@ParentSiteMapId uniqueidentifier = NULL,
	@Title nvarchar(50) = NULL,
	@Description nvarchar(50) = NULL,
	@URL nvarchar(200) = NULL,
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[SiteMap]
	(
		[SiteMapId],
		[ModuleId],
		[ParentSiteMapId],
		[Title],
		[Description],
		[URL],
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@SiteMapId,
		@ModuleId,
		@ParentSiteMapId,
		@Title,
		@Description,
		@URL,
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error
