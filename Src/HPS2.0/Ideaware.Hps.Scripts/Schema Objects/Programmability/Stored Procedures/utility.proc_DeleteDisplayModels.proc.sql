﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteDisplayModels] - Delete Procedure Script for DisplayModels
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [utility].[proc_DeleteDisplayModels]
(
	@DisplayModelId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Utility].[DisplayModels]
	WHERE  
		[DisplayModelId] = @DisplayModelId

	RETURN @@Error
