﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertDocumentTypes] - Insert Procedure Script for DocumentTypes
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertDocumentTypes]
(
	@DocumentTypeId uniqueidentifier,
	@DocumentName varchar(200),
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[DocumentTypes]
	(
		[DocumentTypeId],
		[DocumentName],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@DocumentTypeId,
		@DocumentName,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
