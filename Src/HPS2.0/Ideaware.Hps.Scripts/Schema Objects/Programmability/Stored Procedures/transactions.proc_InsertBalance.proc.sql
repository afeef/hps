﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertBalance] - Insert Procedure Script for Balance
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_InsertBalance]
(
	@BalanceId uniqueidentifier,
	@BookingId uniqueidentifier = NULL,
	@BalanceAmount int = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Transactions].[Balance]
	(
		[BalanceId],
		[BookingId],
		[BalanceAmount],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@BalanceId,
		@BookingId,
		@BalanceAmount,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
