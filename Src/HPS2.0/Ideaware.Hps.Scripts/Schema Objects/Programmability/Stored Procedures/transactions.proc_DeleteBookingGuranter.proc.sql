﻿--|--------------------------------------------------------------------------------
--| [proc_DeleteBookingGuranter] - Delete Procedure Script for BookingGuranter
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_DeleteBookingGuranter]
(
	@BookingGuaranterId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Transactions].[BookingGuranter]
	WHERE  
		[BookingGuaranterId] = @BookingGuaranterId

	RETURN @@Error
