﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertUserModules] - Insert Procedure Script for UserModules
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_InsertUserModules]
(
	@UserModuleId uniqueidentifier,
	@UserId uniqueidentifier = NULL,
	@ModuleId uniqueidentifier,
	@IsLocked bit,
	@AccessUserId uniqueidentifier = NULL,
	@AccessDateTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Users].[UserModules]
	(
		[UserModuleId],
		[UserId],
		[ModuleId],
		[IsLocked],
		[AccessUserId],
		[AccessDateTime]
	)
	VALUES
	(
		@UserModuleId,
		@UserId,
		@ModuleId,
		@IsLocked,
		@AccessUserId,
		@AccessDateTime
	)
	RETURN @@Error
