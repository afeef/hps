﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteModel] - Delete Procedure Script for Model
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeleteModel]
(
	@ModelId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[Model]
	WHERE  
		[ModelId] = @ModelId

	RETURN @@Error
