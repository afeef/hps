﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteManufacturer] - Delete Procedure Script for Manufacturer
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeleteManufacturer]
(
	@ManufacturerId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[Manufacturer]
	WHERE  
		[ManufacturerId] = @ManufacturerId

	RETURN @@Error
