﻿
--|--------------------------------------------------------------------------------
--| [proc_GetColor] - Get Procedure Script for Color
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_GetColor]
(
	@sortExpression NVARCHAR(100),
	@sortDirection CHAR(4),
	@condition NVARCHAR(1000),
	@startRowIndex BIGINT,
	@pageSize INT,
	@totalRecords BIGINT OUTPUT
)
AS

DECLARE @sql NVARCHAR(4000)
DECLARE @totalRecordsSQL NVARCHAR(4000)


--If no sort expression is specified use AccessDateTime as default sort expression
IF LEN(@sortExpression) = 0
	SET @sortExpression = 'AccessDateTime'
	
--If no sort direction is provided then 'ASC'
IF LEN(@sortDirection) = 0
	SET @sortDirection = 'ASC'
	
	
SET @sql = 'SELECT ColorId, ColorName, RowGuid, AccessUserId, TimeStampVal
			FROM (SELECT ColorId, ColorName, RowGuid, AccessUserId,TimeStampVal,
				ROW_NUMBER() OVER (ORDER BY ' + @sortExpression +' '+ @sortDirection + ') AS RowRank
				FROM [Setup].Color'

IF LEN(@condition) <> 0
	SET @sql = @sql + @condition

SET @sql = @sql + ') AS ProductsWithRowNumbers
			WHERE (RowRank >= ' + CONVERT(NVARCHAR(10), @startRowIndex) +
			' AND RowRank < (' + CONVERT(NVARCHAR(10), @startRowIndex) + ' + '
			+ CONVERT(NVARCHAR(10), @pageSize) + '))'

EXEC(@sql)

--Get total no of record
DECLARE @total BIGINT 
SET @totalRecordsSQL = 'SELECT @total = ISNULL(COUNT(*),0) FROM [Setup]. Color'

IF LEN(@condition) <> 0
	SET @totalRecordsSQL = @totalRecordsSQL + @condition

EXEC sp_executesql @totalRecordsSQL,N'@total BIGINT OUTPUT', @total OUTPUT

SET @totalRecords =@total

RETURN @totalRecords	

RETURN @@Error
