﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteUserRights] - Delete Procedure Script for UserRights
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteUserRights]
(
	@UserRightId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[UserRights]
	WHERE  
		[UserRightId] = @UserRightId

	RETURN @@Error
