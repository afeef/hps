﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertBranch] - Insert Procedure Script for Branch
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertBranch]
(
	@BranchId uniqueidentifier,
	@BranchName varchar(150) = NULL,
	@OfficeAddress varchar(300) = NULL,
	@ContactNumber varchar(50) = NULL,
	@FaxNumber varchar(50) = NULL,
	@EmailAddress varchar(30) = NULL,
	@IsBlock bit = NULL,
	@BlockDetail varchar(300) = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[Branch]
	(
		[BranchId],
		[BranchName],
		[OfficeAddress],
		[ContactNumber],
		[FaxNumber],
		[EmailAddress],
		[IsBlock],
		[BlockDetail],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@BranchId,
		@BranchName,
		@OfficeAddress,
		@ContactNumber,
		@FaxNumber,
		@EmailAddress,
		@IsBlock,
		@BlockDetail,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
