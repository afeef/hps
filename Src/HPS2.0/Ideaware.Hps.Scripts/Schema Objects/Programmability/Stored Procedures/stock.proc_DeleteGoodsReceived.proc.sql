﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteGoodsReceived] - Delete Procedure Script for GoodsReceived
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_DeleteGoodsReceived]
(
	@GoodsReceivedId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Stock].[GoodsReceived]
	WHERE  
		[GoodsReceivedId] = @GoodsReceivedId

	RETURN @@Error
