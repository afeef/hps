﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteRoleRights] - Delete Procedure Script for RoleRights
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [users].[proc_DeleteRoleRights]
(
	@RoleRightId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Users].[RoleRights]
	WHERE  
		[RoleRightId] = @RoleRightId

	RETURN @@Error
