﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertBooking] - Insert Procedure Script for Booking
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_InsertBooking]
(
	@BookingId uniqueidentifier,
	@BranchId uniqueidentifier = NULL,
	@CustomerId uniqueidentifier = NULL,
	@ProductId uniqueidentifier = NULL,
	@AccountNo varchar(50) = NULL,
	@BookingDate datetime = NULL,
	@SalePrice int = NULL,
	@ServiceCharges int = NULL,
	@DiscountAmount int = NULL,
	@AdvanceAmount int = NULL,
	@NoOfInstallments int = NULL,
	@ModeOfPayment int = NULL,
	@ChequeNo varchar(50) = NULL,
	@IsActive bit = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Transactions].[Booking]
	(
		[BookingId],
		[BranchId],
		[CustomerId],
		[ProductId],
		[AccountNo],
		[BookingDate],
		[SalePrice],
		[ServiceCharges],
		[DiscountAmount],
		[AdvanceAmount],
		[NoOfInstallments],
		[ModeOfPayment],
		[ChequeNo],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@BookingId,
		@BranchId,
		@CustomerId,
		@ProductId,
		@AccountNo,
		@BookingDate,
		@SalePrice,
		@ServiceCharges,
		@DiscountAmount,
		@AdvanceAmount,
		@NoOfInstallments,
		@ModeOfPayment,
		@ChequeNo,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
