﻿
--|--------------------------------------------------------------------------------
--| [proc_DeleteDocumentTypes] - Delete Procedure Script for DocumentTypes
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_DeleteDocumentTypes]
(
	@DocumentTypeId uniqueidentifier
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Setup].[DocumentTypes]
	WHERE  
		[DocumentTypeId] = @DocumentTypeId

	RETURN @@Error
