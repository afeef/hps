﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertDisplayModels] - Insert Procedure Script for DisplayModels
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [utility].[proc_InsertDisplayModels]
(
	@DisplayModelId uniqueidentifier,
	@ModelId uniqueidentifier = NULL,
	@ModelYear int = NULL,
	@ModelPicture varbinary(2000) = NULL,
	@IsActive bit = NULL,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Utility].[DisplayModels]
	(
		[DisplayModelId],
		[ModelId],
		[ModelYear],
		[ModelPicture],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@DisplayModelId,
		@ModelId,
		@ModelYear,
		@ModelPicture,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
