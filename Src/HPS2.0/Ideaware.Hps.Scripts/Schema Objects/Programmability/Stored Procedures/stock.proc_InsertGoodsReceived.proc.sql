﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertGoodsReceived] - Insert Procedure Script for GoodsReceived
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [stock].[proc_InsertGoodsReceived]
(
	@GoodsReceivedId uniqueidentifier,
	@CategoryId uniqueidentifier = NULL,
	@VendorId uniqueidentifier = NULL,
	@GoodsReceivedNo varchar(50) = NULL,
	@GoodsReceivedDate datetime = NULL,
	@Remarks varchar(1000) = NULL,
	@IsActive bit,
	@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Stock].[GoodsReceived]
	(
		[GoodsReceivedId],
		[CategoryId],
		[VendorId],
		[GoodsReceivedNo],
		[GoodsReceivedDate],
		[Remarks],
		[IsActive],
		[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@GoodsReceivedId,
		@CategoryId,
		@VendorId,
		@GoodsReceivedNo,
		@GoodsReceivedDate,
		@Remarks,
		@IsActive,
		@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
