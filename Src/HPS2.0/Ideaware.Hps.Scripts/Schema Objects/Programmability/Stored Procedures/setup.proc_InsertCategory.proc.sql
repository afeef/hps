﻿
--|--------------------------------------------------------------------------------
--| [proc_InsertCategory] - Insert Procedure Script for Category
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [setup].[proc_InsertCategory]
(
	@CategoryId uniqueidentifier,
	@CategoryName varchar(150) = NULL,
	@ColorId bit= NULL,
	@EngineNo bit= NULL,
	@ChasisNo bit= NULL,
	@RegistrationNo bit= NULL,
	@ProductSize bit= NULL,
	@ProductWeight bit= NULL,
	@EMINo bit= NULL,
	@ManufacturingDate bit= NULL,
	@ValidityDate bit= NULL,
	@KeyNo bit= NULL,
	--@RowGuid uniqueidentifier = NULL,
	@AccessUserId uniqueidentifier = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Setup].[Category]
	(
		[CategoryId],
		[CategoryName],
		[ColorId],
		[EngineNo],
		[ChasisNo],
		[RegistrationNo],
		[ProductSize],
		[ProductWeight],
		[EMINo],
		[ManufacturingDate],
		[ValidityDate],
		[KeyNo],
		--[RowGuid],
		[AccessUserId]
	)
	VALUES
	(
		@CategoryId,
		@CategoryName,
		@ColorId,
		@EngineNo,
		@ChasisNo,
		@RegistrationNo,
		@ProductSize,
		@ProductWeight,
		@EMINo,
		@ManufacturingDate,
		@ValidityDate,
		@KeyNo,
		--@RowGuid,
		@AccessUserId
	)
	RETURN @@Error
