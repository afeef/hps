﻿
--|--------------------------------------------------------------------------------
--| [proc_UpdateInstallmentCredit] - Update Procedure Script for InstallmentCredit
--|--------------------------------------------------------------------------------
CREATE PROCEDURE [transactions].[proc_UpdateInstallmentCredit]
(
	@UpdateSet	NVARCHAR(4000),
	@Key UNIQUEIDENTIFIER,
	@TimeStampVal TIMESTAMP
)
AS

	SET NOCOUNT ON
	DECLARE @sql VARCHAR(4000)
	Declare @table_time_stamp as timestamp
-- Get existing TimeStamp	
	SELECT	@table_time_stamp = TimeStampVal 
	FROM [Transactions].[InstallmentCredit]
	WHERE 
		[InstallmentCreditId] = @Key	

-- Update selective columns	if timestamp is equal to existing timestamp (to avoid concurrency problems)
	IF @table_time_stamp = @TimeStampVal
	BEGIN
		SET @sql = 'UPDATE [Transactions].[InstallmentCredit] SET ' + @UpdateSet + ' WHERE 
				[InstallmentCreditId] ='''+ CAST(@Key AS VARCHAR(38))+''''

		EXEC(@sql)
	END
	ELSE
	BEGIN
		RAISERROR('Concurrency Violation', 25, 1)
	END

