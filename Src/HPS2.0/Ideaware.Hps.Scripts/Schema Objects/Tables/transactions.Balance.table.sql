﻿CREATE TABLE [transactions].[Balance] (
    [BalanceId]     INT      IDENTITY (1, 1) NOT NULL,
    [BookingId]     INT      NULL,
    [BalanceAmount] INT      NULL,
    [DateCreated]   DATETIME NULL,
    [DateModified]  DATETIME NULL,
    [IsActive]      BIT      NULL,
    [AccessUserId]  INT      NULL
);

