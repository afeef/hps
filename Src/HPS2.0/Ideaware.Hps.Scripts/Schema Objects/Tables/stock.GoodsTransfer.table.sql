﻿CREATE TABLE [stock].[GoodsTransfer] (
    [GoodsTransferId]      INT           IDENTITY (1, 1) NOT NULL,
    [GoodsTransferNo]      VARCHAR (50)  NULL,
    [GoodsTransferDate]    DATETIME      NULL,
    [GoodsTransferFrom]    INT           NULL,
    [GoodsTransferTo]      INT           NULL,
    [GoodsTransferRemarks] VARCHAR (500) NULL,
    [DateCreated]          DATETIME      NULL,
    [DateModified]         DATETIME      NULL,
    [IsActive]             BIT           NOT NULL,
    [AccessUserId]         INT           NULL
);

