﻿CREATE TABLE [utility].[DisplayModels] (
    [DisplayModelId] UNIQUEIDENTIFIER NOT NULL,
    [ModelId]        UNIQUEIDENTIFIER NULL,
    [ModelYear]      INT              NULL,
    [ModelPicture]   VARBINARY (2000) NULL,
    [IsActive]       BIT              NULL,
    [RowGuid]        UNIQUEIDENTIFIER NULL,
    [AccessUserId]   UNIQUEIDENTIFIER NULL,
    [TimeStampVal]   TIMESTAMP        NULL
);

