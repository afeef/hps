﻿ALTER TABLE [stock].[GoodsTransfer]
    ADD CONSTRAINT [PK_GoodsTransfer_1] PRIMARY KEY CLUSTERED ([GoodsTransferId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

