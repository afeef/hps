﻿CREATE TABLE [setup].[DocumentTypes] (
    [DocumentTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [DocumentName]   VARCHAR (200) NOT NULL,
    [DateCreated]    DATETIME      NULL,
    [DateModified]   DATETIME      NULL,
    [IsActive]       BIT           NULL,
    [AccessUserId]   INT           NULL,
    CONSTRAINT [PK_DocumentTypes] PRIMARY KEY CLUSTERED ([DocumentTypeId] ASC)
);



