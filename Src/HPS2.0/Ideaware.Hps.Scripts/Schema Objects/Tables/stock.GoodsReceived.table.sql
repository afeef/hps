﻿CREATE TABLE [stock].[GoodsReceived] (
    [GoodsReceivedId]   INT            IDENTITY (1, 1) NOT NULL,
    [VendorId]          INT            NULL,
    [CategoryId]        INT            NULL,
    [GoodsReceivedNo]   VARCHAR (50)   NULL,
    [GoodsReceivedDate] DATETIME       NULL,
    [Remarks]           VARCHAR (1000) NULL,
    [DateCreated]       DATETIME       NULL,
    [DateModified]      DATETIME       NULL,
    [IsActive]          BIT            CONSTRAINT [DF_Grn_is_active] DEFAULT ((1)) NULL,
    [AccessUserId]      INT            NULL,
    CONSTRAINT [PK_GoodsReceived_1] PRIMARY KEY CLUSTERED ([GoodsReceivedId] ASC),
    CONSTRAINT [FK_GoodsReceived_Categories] FOREIGN KEY ([CategoryId]) REFERENCES [setup].[Categories] ([CategoryId]),
    CONSTRAINT [FK_GoodsReceived_Vendors] FOREIGN KEY ([VendorId]) REFERENCES [setup].[Vendors] ([VendorId])
);



