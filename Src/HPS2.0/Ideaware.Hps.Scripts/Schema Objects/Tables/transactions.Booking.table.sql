﻿CREATE TABLE [transactions].[Booking] (
    [BookingId]        INT          IDENTITY (1, 1) NOT NULL,
    [BranchId]         INT          NULL,
    [CustomerId]       INT          NULL,
    [ProductId]        INT          NULL,
    [AccountNo]        VARCHAR (50) NULL,
    [BookingDate]      DATETIME     NULL,
    [SalePrice]        INT          NULL,
    [ServiceCharges]   INT          NULL,
    [DiscountAmount]   INT          NULL,
    [AdvanceAmount]    INT          NULL,
    [NoOfInstallments] INT          NULL,
    [ModeOfPayment]    INT          NULL,
    [ChequeNo]         VARCHAR (50) NULL,
    [DateCreated]      DATETIME     NULL,
    [DateModified]     DATETIME     NULL,
    [IsActive]         BIT          NULL,
    [AccessUserId]     INT          NULL,
    CONSTRAINT [PK_Booking] PRIMARY KEY CLUSTERED ([BookingId] ASC),
    CONSTRAINT [FK_Booking_Branches] FOREIGN KEY ([BranchId]) REFERENCES [setup].[Branches] ([BranchId]),
    CONSTRAINT [FK_Booking_Product] FOREIGN KEY ([ProductId]) REFERENCES [stock].[Product] ([ProductId]),
    CONSTRAINT [FK_Booking_Users] FOREIGN KEY ([CustomerId]) REFERENCES [security].[Users] ([UserId])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1 = cash, 2=checque, 3=prize bond, 4=Traveller Tickets, 5=Bank Draft', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'Booking', @level2type = N'COLUMN', @level2name = N'ModeOfPayment';

