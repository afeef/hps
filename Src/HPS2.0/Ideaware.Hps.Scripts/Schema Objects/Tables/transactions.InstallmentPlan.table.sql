﻿CREATE TABLE [transactions].[InstallmentPlan] (
    [InstallmentPlanId] INT      IDENTITY (1, 1) NOT NULL,
    [ParentPlanId]      INT      NULL,
    [BookingId]         INT      NULL,
    [InstallmentNo]     INT      NULL,
    [InstallmentDate]   DATETIME NULL,
    [InstallmentAmount] INT      NULL,
    [ProfitAmount]      INT      NULL,
    [IsComplete]        BIT      NULL,
    [DateCreated]       DATETIME NULL,
    [DateModified]      DATETIME NULL,
    [IsActive]          BIT      NULL,
    [AccessUserId]      INT      NULL,
    CONSTRAINT [PK_InstallmentPlan_1] PRIMARY KEY CLUSTERED ([InstallmentPlanId] ASC),
    CONSTRAINT [FK_InstallmentPlan_Booking] FOREIGN KEY ([BookingId]) REFERENCES [transactions].[Booking] ([BookingId])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'it defines the decided date of the installement', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'InstallmentPlan', @level2type = N'COLUMN', @level2name = N'InstallmentDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'it will be false if installement is not completed', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'InstallmentPlan', @level2type = N'COLUMN', @level2name = N'IsComplete';

