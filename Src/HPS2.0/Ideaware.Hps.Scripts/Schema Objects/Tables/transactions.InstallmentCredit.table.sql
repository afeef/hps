﻿CREATE TABLE [transactions].[InstallmentCredit] (
    [InstallmentCreditId] INT           IDENTITY (1, 1) NOT NULL,
    [BranchId]            INT           NULL,
    [BookingId]           INT           NULL,
    [InstallmentPlanId]   INT           NULL,
    [ReceiptNo]           VARCHAR (50)  NULL,
    [PaymentDate]         DATETIME      NULL,
    [AmountPaid]          INT           NULL,
    [FinePaid]            INT           NULL,
    [ModeOfPayment]       VARCHAR (50)  NULL,
    [ChequeDetail]        VARCHAR (150) NULL,
    [InstallmentRemarks]  VARCHAR (300) NULL,
    [DateCreated]         DATETIME      NULL,
    [DateModified]        DATETIME      NULL,
    [IsActive]            BIT           CONSTRAINT [DF_InstallmentCredit_is_active] DEFAULT ((1)) NULL,
    [AccessUserId]        INT           NULL,
    CONSTRAINT [PK_InstallmentCredit] PRIMARY KEY CLUSTERED ([InstallmentCreditId] ASC),
    CONSTRAINT [FK_InstallmentCredit_Booking] FOREIGN KEY ([BookingId]) REFERENCES [transactions].[Booking] ([BookingId]),
    CONSTRAINT [FK_InstallmentCredit_Branches] FOREIGN KEY ([BranchId]) REFERENCES [setup].[Branches] ([BranchId]),
    CONSTRAINT [FK_InstallmentCredit_InstallmentPlan] FOREIGN KEY ([InstallmentPlanId]) REFERENCES [transactions].[InstallmentPlan] ([InstallmentPlanId])
);



