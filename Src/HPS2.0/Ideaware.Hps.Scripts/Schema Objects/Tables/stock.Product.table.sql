﻿CREATE TABLE [stock].[Product] (
    [ProductId]         INT           IDENTITY (1, 1) NOT NULL,
    [GoodsReceivedId]   INT           NULL,
    [ManufacturerId]    INT           NULL,
    [ModelId]           INT           NULL,
    [BranchId]          INT           NULL,
    [ColorId]           INT           NULL,
    [ProductNo]         VARCHAR (50)  NULL,
    [ProductName]       VARCHAR (300) NULL,
    [EngineNo]          VARCHAR (100) NULL,
    [ChasisNo]          VARCHAR (100) NULL,
    [RegistrationNo]    VARCHAR (15)  NULL,
    [ProductSize]       VARCHAR (50)  NULL,
    [ProductWeight]     VARCHAR (50)  NULL,
    [EMINo]             VARCHAR (50)  NULL,
    [ManufacturingDate] DATETIME      NULL,
    [ValidityDate]      DATETIME      NULL,
    [KeyNo]             VARCHAR (100) NULL,
    [ActualPrice]       INT           NULL,
    [SalePrice]         INT           NULL,
    [ProductStatus]     INT           NULL,
    [ProductRemarks]    VARCHAR (500) NULL,
    [IsActive]          BIT           CONSTRAINT [DF_Product_is_active] DEFAULT ((1)) NULL,
    [DateCreated]       DATETIME      NULL,
    [DateModified]      DATETIME      NULL,
    [AccessUserId]      INT           NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC),
    CONSTRAINT [FK_Product_Branches] FOREIGN KEY ([BranchId]) REFERENCES [setup].[Branches] ([BranchId]),
    CONSTRAINT [FK_Product_Colors] FOREIGN KEY ([ColorId]) REFERENCES [setup].[Colors] ([ColorId]),
    CONSTRAINT [FK_Product_GoodsReceived] FOREIGN KEY ([GoodsReceivedId]) REFERENCES [stock].[GoodsReceived] ([GoodsReceivedId]),
    CONSTRAINT [FK_Product_Manufacturers] FOREIGN KEY ([ManufacturerId]) REFERENCES [setup].[Manufacturers] ([ManufacturerId]),
    CONSTRAINT [FK_Product_Models] FOREIGN KEY ([ModelId]) REFERENCES [setup].[Models] ([ModelId])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'branch id shows that where currently this product exists', @level0type = N'SCHEMA', @level0name = N'stock', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'BranchId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1 for NEW, 2 for RESHEDULED, 3 for USED, 4 for Sold', @level0type = N'SCHEMA', @level0name = N'stock', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'ProductStatus';

