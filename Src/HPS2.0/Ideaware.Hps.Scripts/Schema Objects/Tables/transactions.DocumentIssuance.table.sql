﻿CREATE TABLE [transactions].[DocumentIssuance] (
    [DocumentIssuanceId] INT           IDENTITY (1, 1) NOT NULL,
    [BranchId]           INT           NULL,
    [DocumentTypeId]     INT           NULL,
    [IssuanceDate]       DATETIME      NULL,
    [DocumentNo]         VARCHAR (100) NULL,
    [CopyStatus]         INT           NULL,
    [NumberOfCopies]     INT           NULL,
    [ReceivingPerson]    VARCHAR (100) NULL,
    [IssuedBy]           VARCHAR (100) NULL,
    [DateCreated]        INT           NULL,
    [DateModified]       INT           NULL,
    [IsActive]           BIT           NULL,
    [AccessUserId]       INT           NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identity of branch', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'BranchId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'which type of document is being issued like Registration Book etc', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'DocumentTypeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'date when document was issued', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'IssuanceDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'document number if any', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'DocumentNo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1= Orignal , 2= Photocopy', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'CopyStatus';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'how many copies are being issued, if data entry operator select copy_status = Photocopy', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'NumberOfCopies';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'who recieved document, by default name of booking person will appear', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'ReceivingPerson';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'who is issuing documents', @level0type = N'SCHEMA', @level0name = N'transactions', @level1type = N'TABLE', @level1name = N'DocumentIssuance', @level2type = N'COLUMN', @level2name = N'IssuedBy';

