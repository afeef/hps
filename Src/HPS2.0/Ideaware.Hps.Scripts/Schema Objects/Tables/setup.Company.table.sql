﻿CREATE TABLE [setup].[Company] (
    [CompanyId]          INT           NOT NULL,
    [CompanyName]        NVARCHAR (50) NULL,
    [CompanyDescription] NVARCHAR (50) NULL,
    [DateCreated]        DATETIME      NULL,
    [DateModified]       DATETIME      NULL,
    [IsActive]           BIT           NULL,
    [AccessUserId]       INT           NULL
);

