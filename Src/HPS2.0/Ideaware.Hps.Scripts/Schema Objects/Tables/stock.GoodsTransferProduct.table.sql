﻿CREATE TABLE [stock].[GoodsTransferProduct] (
    [GoodsTransferProductId] INT IDENTITY (1, 1) NOT NULL,
    [GoodsTransferId]        INT NULL,
    [ProductId]              INT NULL,
    CONSTRAINT [PK_GoodsTransferProduct] PRIMARY KEY CLUSTERED ([GoodsTransferProductId] ASC),
    CONSTRAINT [FK_GoodsTransferProduct_GoodsTransfer] FOREIGN KEY ([GoodsTransferId]) REFERENCES [stock].[GoodsTransfer] ([GoodsTransferId]),
    CONSTRAINT [FK_GoodsTransferProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [stock].[Product] ([ProductId])
);



