﻿CREATE TABLE [security].[Permissions] (
    [PermissionId]          INT            IDENTITY (1, 1) NOT NULL,
    [PermissionName]        NVARCHAR (50)  NOT NULL,
    [PermissionFlag]        INT            NULL,
    [PermissionDescription] NVARCHAR (250) NULL,
    [DateCreated]           DATETIME       NULL,
    [DateModified]          DATETIME       NULL,
    [IsActive]              BIT            NULL,
    [AccessUserId]          INT            NULL,
    CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);



