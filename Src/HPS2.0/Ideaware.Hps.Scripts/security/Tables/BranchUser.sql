﻿CREATE TABLE [security].[BranchUser] (
    [BranchUserId] INT IDENTITY (1, 1) NOT NULL,
    [BranchId]     INT NOT NULL,
    [UserId]       INT NOT NULL,
    CONSTRAINT [PK_BranchUser] PRIMARY KEY CLUSTERED ([BranchUserId] ASC),
    CONSTRAINT [FK_BranchUser_Branch] FOREIGN KEY ([BranchId]) REFERENCES [setup].[Branches] ([BranchId]),
    CONSTRAINT [FK_BranchUser_Users] FOREIGN KEY ([UserId]) REFERENCES [security].[Users] ([UserId])
);

