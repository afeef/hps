﻿CREATE TABLE [security].[RolePermissions] (
    [RolePermissionId] INT IDENTITY (1, 1) NOT NULL,
    [RoleId]           INT NOT NULL,
    [PermissionId]     INT NOT NULL,
    CONSTRAINT [PK_RolePermissions] PRIMARY KEY CLUSTERED ([RolePermissionId] ASC),
    CONSTRAINT [FK_RolePermissions_Permissions] FOREIGN KEY ([PermissionId]) REFERENCES [security].[Permissions] ([PermissionId]),
    CONSTRAINT [FK_RolePermissions_Roles] FOREIGN KEY ([RoleId]) REFERENCES [security].[Roles] ([RoleId])
);

