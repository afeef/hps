﻿CREATE TABLE [security].[Roles] (
    [RoleId]          INT            IDENTITY (1, 1) NOT NULL,
    [RoleName]        NVARCHAR (256) NOT NULL,
    [LoweredRoleName] NVARCHAR (256) NOT NULL,
    [Description]     NVARCHAR (256) NULL,
    [DateCreated]     DATETIME       NULL,
    [DateModified]    DATETIME       NULL,
    [IsActive]        BIT            NULL,
    [AccessUserId]    INT            NULL,
    CONSTRAINT [PK_Roles_1] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);

